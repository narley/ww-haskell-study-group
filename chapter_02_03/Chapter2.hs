module Chapter2 where


-- 2.2

sayHello :: String -> IO ()
sayHello x =
  putStrLn ("Hello " ++ x ++ "!")

-- 2.4

triple x = x * 3

-- Exercises: Comprehension Check

-- 1
half x = x / 2

square x = x * x

addHalfSquare x y = half x + square y


-- 2
multPi x = 3.14 * x


-- 3
multPi' x = pi * x


-- Exercises: Parentheses and Association

-- 1
a1 = 8 + 7 * 9
b1 = (8 + 7) * 9
-- produce different results

-- 2
perimeter x y = (x * 2) + (y * 2)
perimeter' x y = x * 2 + y * 2
-- produce same results

-- 3
f x = x / 2 + 9
f' x = x / (2 + 9)
-- produce different results


-- Exercises: Heal the Sick

-- 1
-- area x = 3. 14 * (x * x)
area x = 3.14 * (x * x)

-- 2
-- double x = b * 2
double x = x * 2

-- 3
-- x = 7
--  y = 10
-- f3 = x + y
x = y
y = 10
f3 = x + y


-- Exercises: A Head Code

-- 1
-- let x = 5 in x
let2Where1 = x
  where x = 5

-- 2
-- let x = 5 in x * x
let2Where2 = x * x
  where x = 5

-- 3
-- let x = 5; y = 6 in x * y
let2Where3 = x * y
  where x = 5
        y = 6

-- 4
-- let x = 3; y = 1000 in x + 3
let2Where4 = x + 3
  where x = 3
        y = 1000

-- 5
-- let x = 3; y = 1000 in x * 3 + y
let2Where5 = x * 3 + y
  where x = 3
        y = 1000

-- 6
-- let y = 10; x = 10 * 5 + y in x * 5
let2Where6 = x * 5
  where y = 10
        x = 10 * 5 + y

-- 7
-- let x = 7; y = negate x; z = y * 10 in z / x + y
let2Where7 = z / x + y
  where x = 7
        y = negate x
        z = y * 10


-- Chapter Exercises

-- Parenthesization

-- 0
-- 2 + 2 * 3 - 3
-- 2 + (2 * 3) - 3

-- 1
-- 2 + 2 * 3 - 1
-- 2 + (2 * 3) - 1

-- 2
-- (^) 10 $ 1 + 1
-- ((^) 10) $ (1 + 1)

-- 3
-- 2 ^ 2 * 4 ^ 5 + 1
-- ((2 ^ 2) * (4 ^ 5)) + 1


-- Equivalent Expressions

-- 1
-- 1 + 1 == 2 => true

-- 2
-- 10 ^ 2 == 10 + 9 * 10 => true

-- 3
-- 400 - 37 == (-) 37 400 => false

-- 4
-- 100 `div` 3 == 100 / 3 => false

-- 5
-- 2 * 5 + 18 == 2 * (5 + 18) => false


-- More Fun With Functions

z' = 7
x' = y' ^ 2
waxOn = x' * 5 -- 1125
y' = z' + 8

-- 1
waxOnAdd10 = 10 + waxOn
waxOnAdd10' = (+10) waxOn
waxOnMinus15 = (-) 15 waxOn
waxOnMinus15' = (-) waxOn 15

--3
tripleWaxOn = triple waxOn

--4
waxOn' = x'' * 5
  where
    x'' = y'' ^ 2
    y'' = z'' + 8
    z'' = 7

-- 6
waxOff x = triple x
