module Chapter3 where

import Prelude

-- 3.3

main :: IO ()
main = do
  putStrLn "Count to four for me:"
  putStr "one, two"
  putStr ", three, and"
  putStrLn " four!"

mainWithoutDo :: IO ()
mainWithoutDo =
  putStrLn "Count to four for me:" >>=
  \() -> putStr "one, two" >>=
  \() -> putStr ", three and" >>=
  \() -> putStrLn " four!"

main' :: IO ()
main' = do
  putStr "Count to four for me:"
  putStrLn "one, two"
  putStrLn ", three and"
  putStr " four"

mainWithoutDo' :: IO ()
mainWithoutDo' =
  putStr "Count to four for me:" >>=
  \() -> putStrLn "one, two" >>=
  \() -> putStrLn ", three and" >>=
  \() -> putStr " four"

myGreeting :: String
myGreeting = "hello" ++ " world!"

hello :: String
hello = "hello"

world :: [Char]
world = "world!"

printHelloWorld :: IO ()
printHelloWorld = do
  putStrLn myGreeting
  putStrLn secondGreeting
  where
    secondGreeting =
      concat [hello, " ", world]

printHelloWorldWithoutDo :: IO ()
printHelloWorldWithoutDo =
  putStrLn myGreeting >>=
  \() -> putStrLn secondGreeting
  where
    secondGreeting =
      concat [hello, " ", world]

-- 3.4

topLevelFunction :: Integer -> Integer
topLevelFunction x =
  x + woot + topLevelValue
  where
    woot :: Integer
    woot = 10

topLevelValue :: Integer
topLevelValue = 5


-- Exercises: Scope

-- 1
-- Prelude> x = 5
-- Prelude> y = 7
-- Prelude> z = x * y
-- Yes, y is in scope for z

-- 2
-- Prelude> f = 3
-- Prelude> g = 6 * f + h
-- No, h is not in scope for g

-- 3
-- area d = pi * (r * r)
-- r = d / 2
-- No, d needs to be defined

-- 4
-- area d = pi * (r * r)
--   where r = d / 2
-- Yes, r and d are in scope


-- Exercises: Syntax Errors

-- 1
-- ++ [1,2,3] [4,5,6] -- error
-- (++) [1,2,3] [4,5,6]

-- 2
-- '<3' ++ ' Haskell' -- error
-- "<3" ++ " Haskell"

-- 3
-- concat ["<3", " Haskel"] -- correct


-- 3.6

myGreeting' :: String
myGreeting' = (++) "hello" " world!"

hello' :: String
hello' = "hello"

world' :: String
world' = "world"

main36 :: IO ()
main36 = do
  putStrLn myGreeting'
  putStrLn secondGreeting
  where
    secondGreeting =
      (++) hello ((++) " " world)

main36WithoutDo :: IO ()
main36WithoutDo =
  putStrLn myGreeting' >>=
  \() -> putStrLn secondGreeting
  where
    secondGreeting =
      (++) hello $ (++) " " world


printSecond :: IO ()
printSecond =
  putStrLn greeting

main36' :: IO ()
main36' = do
  putStrLn greeting
  printSecond

main36WithoutDo' :: IO ()
main36WithoutDo' =
  putStrLn greeting >>=
  \() -> printSecond

greeting :: String
greeting = "Yarrrrr"
