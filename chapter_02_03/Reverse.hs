module Reverse where


rvrs :: String -> String
rvrs s =
  concat [ drop 9 s
         , " "
         , drop 6 $ take 9 s
         , take 5 s
         ]

main :: IO ()
main = print $ rvrs "Curry is awesome"
