module ExtraExercises where


-- Expression forms

-- 1) 23 == normal
-- 2) (2,3) == normal
-- 3) \x -> 2 + 3 == weak
-- 4) (\x -> x + 2) 3 == redex
-- 5) (\x -> \y -> x + y) 2 == weak
-- 6) 2 + 3 == redex
-- 7) (2 + 2, 3 + 3) == weak
-- 8) Just (2 + 3) == weak
-- 9) Just 5 == normal
-- 10) [1,2,3] ++ [4,5,6] == redex
-- 11) Just $ [1,2,3] ++ [4,5,6] == weak
-- 12) [Just 1, Just 2, Just (2 + 3)] == weak
-- 13) [Just (1,2), Just(2,3), Just (4,5)] == normal
-- 14) \x -> \y -> \z -> x + y(z) == weak
-- 15) \x -> ((\y -> y + 1) 1) + x == weak
-- 16) [[1,2,3]] == normal
-- 17) [1,2,3] : [] == weak
-- 18) [1 + 1] == weak
-- 19) [1] ++ [] == redex
-- 20) (+) 1 == weak
-- 21) (+) 1 2 == redex
-- 22) print == weak
-- 23) div (1 + 2 * 3 - 4) == weak
-- 24) (+ 2) == weak
-- 25) (- 3) == normal


-- Application operator
-- 1
-- take 1 (drop 1 [1..10])
-- take 1 $ drop 1 [1..10]

-- 2
-- fst (swap (swap (2,3)))
-- fst $ swap $ swap (2,3)

-- 3
-- unwords (words (str ++ "!"))
-- unwords $ words $ str ++ "!"

-- 4-- intersect (nub (union [1,2,2,3,4,5,5,5,6,6,7] [2,4,6,8])) [2,4,6,8]
-- intersect (nub $ union [1,2,2,3,4,5,5,5,6,6,7] [2,4,6,8]) [2,4,6,8] N

-- 5
-- map snd (zip (zipWith ($) [(+3), (+2), (+1)] [1,2,3]) "abc")
-- map snd $ zip (zipWith ($) [(+3), (+2), (+1)] [1,2,3]) "abc"


-- let vs. where

-- 1
-- whiteLines colors = let w = "white" in intersperse w colors
-- whiteLines colors = intersperse w colors
--   where w = "white"

-- 2
-- abs n = let n' = negate n in if n < then n' else n
-- abs n = if n < then n' else n
--   where n' = negate n

-- 3
-- f x y z = let a = x * (y - z) in a <= 0
-- f x y z = a < = 0
--   where a = x * (y - z)
