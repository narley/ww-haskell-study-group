module ChapterExercises where

-- Reading Syntax

-- 1
-- a) concat [[1,2,3], [4,5,6]] -- correct

-- b) ++ [1,2,3] [4,5,6] -- wrong
--    (++) [1,2,3] [4,5,6]

-- c) (++) "hello" " world" -- correct

-- d) ["hello" ++ " world"] -- correct

-- e) 4 !! "hello" -- wrong
--    "hello" !! 4

-- f) (!!) "hello" 4 -- correct

-- g) take 3 "awesome" -- correct


-- 2
-- a) concat [[1 * 6], [2 * 6], [3 * 6]]
--    == d) [6, 12, 18]

-- b) "rain" ++ drop 2 "elbow"
--    == c) "rainbow"

-- c) 10 * head [1,2,3]
--    == e) 10

-- d) (take 3 "Julie") ++ (tail "yes")
--    == a) "Jules"

-- e) concat [tail [1,2,3], tail [4,5,6], tail [7,8,9]]
--    == b) [2,3,5,6,8,9]


-- Building Functions

-- 1
-- ex.:
-- input: "Hello World"
-- output: "ello World"
example1 :: String -> String
example1 = drop 1

-- a
-- input: "Curry is awesome"
-- output: "Curry is awesome!"
exeA :: String
exeA = "Curry is awesome" ++ "!"

-- b
-- input: "Curry is awesome!"
-- output: "y"
exeB :: String
exeB = "Curry is awesome!" !! 4 : ""

-- c
-- input: "Curry is awesome!"
-- output: "awesome!"
exeC :: String
exeC = drop 9 "Curry is awesome!"


-- 2
-- a
exeA' :: String -> String
exeA' s = s ++ "!"

-- b
exeB' :: String -> String
exeB' s = s !! 4 : ""

-- c
exeC' :: String -> String
exeC' = drop 9


-- 3
thirdLetter :: String -> Char
thirdLetter s = s !! 3

-- 4
letterIndex :: Int -> Char
letterIndex i = "Curry is awesome!" !! i

-- 5
-- from "Curry is awesome" to "awesome is Curry"
-- use only take and drop
rvrs :: String
rvrs =
  concat [ drop 9 w
         , " "
         , drop 6 $ take 9 w
         , take 5 w
         ]
  where
    w = "Curry is awesome"
