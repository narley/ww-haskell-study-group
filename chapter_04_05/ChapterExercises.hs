{-# LANGUAGE NoMonomorphismRestriction #-}

module ChapterExercises where


-- Chapter 4

awesome :: [String]
awesome = ["Papuchon", "curry", ":)"]

also :: [String]
also = ["Quake", "The Simons"]

allAwesome :: [[String]]
allAwesome = [awesome, also]

-- 1
-- length :: [a] -> Integer => not quite right
-- length :: Foldable t => t a -> Int

-- 2
-- a) length [1,2,3,4,5] => 5
-- b) length [(1,2), (2,3), (3,4)] => 3
-- c) length allAwesome => 2
-- d) length (concat allAwesome) => 5

-- 3
-- 6 / 3 => works
-- 6 / length [1,2,3] => doesn't work because length returns Int and (/) needs a Fractional

-- 4
-- 6 `div` length [1,2,3]

-- 5
-- Type is Bool and result is True

-- 6
-- Type is Bool and result is False

-- 7
-- length allAwesome == 2 => works, returns True. Because allAwesome has 2 items.
-- length [1, 'a', 3, 'b'] => doesn't work. Because array items a of different types.
-- length allAwesome + length awesome => works, returns 5. Because type returned from length is Int it implements Num.
-- (8==8) && ('b' < 'a') => works, returns False. 'b' comes after 'a' therefore is False.
-- (8 == 8) && 9 => doesn't work. Because 9 doesn't reduce a Bool (&& requires both arguments to be Bool).

-- 8
palindrome :: String -> Bool
palindrome s = s == reverse s

-- 9
myAbs :: Integer -> Integer
myAbs x =
  if x < 0 then
    - (x)
  else
    x

-- 10
f :: (a,b) -> (c, d) -> ((b, d), (a, c))
f t1 t2 = ((snd t1, snd t2), (fst t1, fst t2))


-- Correct Syntax

-- 1
x :: Int -> Int -> Int
x = (+)

func1 :: String -> Int
func1 xs = w `x` 1
  where w = length xs

-- 2
id :: a -> a
id x' = x'

-- 3
f' :: (a, b) -> a
f' (a, _) = a


-- Match the function names to their types

-- 1 => c
-- 2 => b
-- 3 => a
-- 4 => d


-- Chapter 5

-- Multiple choice

-- 1 => c
-- 2 => a
-- 3 => b
-- 4 => c

-- Determine the type

-- 1
-- a
multByNine :: Num a => a
multByNine = (* 9) 6

-- b
headB :: Num a => (a, [Char])
headB = head [(0, "doge"),(1, "kitteh")]

-- c
headC :: (Integer, [Char])
headC = head [(0 :: Integer, "doge"),(1, "kitteh")]

-- d
d :: Bool
d = if False then True else False

-- e
lengthE :: Int
lengthE = length [1,2,3,4,5]

-- f
lengthF :: Bool
lengthF = (length [1,2,3,4]) > (length "TACOCAT")


-- 2
x2 = 5
y2 = x2 + 5
w2 :: Num a => a
w2 = y2 * 10

-- 3
x3 = 5
y3 = x3 + 5
z3 :: Num a => a -> a
z3 y = y * 10

-- 4
x4 = 5
y4 = x4 + 5
f4 :: Fractional a => a
f4 = 4 / y4

-- 5
x5 = "Julie"
y5 = " <3 "
z5 = "Haskell"
f5 :: [Char]
f5 = x5 ++ y5 ++ z5

-- Does it compile?

-- 1
-- bigNum = (^) 5 $ 10
bigNum = (^) 5
wahoo = bigNum $ 10

-- 2
-- didn't squawk
x2' = print
y = print "woohoo!"
z = x2' "hello world"

-- 3
a3 = (+)
b3 = 5
-- c3 = b 10
c3 = a3 b3 10
-- d3 = c3 200
d3 = a3 c3 200

-- 4
a4 = 12 + b4
b4 = 10000 * c4
-- missing c4
c4 = 1

-- Type variable or specific type constructor

-- 1
-- f :: Num a => a -> b -> Int -> Int
--                [0]  [1]   [2]    [3]
-- [0] => constrained polymorphic
-- [1] => fully polymorphic
-- [2], [3] => concrete

-- 2
-- f :: zed -> Zed -> Blah
--        [0]    [1]    [2]
-- [0] => fully polymorphic
-- [1], [2] => concrete

-- 3
-- f :: Enum b => a -> b -> c
--                 [0]  [1]  [2]
-- [0], [2] => fully polymorphic
-- [1] => constrained polymorphic

-- 4
-- f :: f -> g -> c
--     [0]  [1]  [2]
-- [0], [1], [2] => fully polymorphic

-- Write type signature

-- 1
functionH :: [a] -> a
functionH (x1':_) = x1'

-- 2
functionC :: (Ord a, Eq a) => a -> a -> Bool
functionC x y =
  if (x > y) then True else False

-- 3
functionS :: (a, b) -> b
functionS (x, y) = y

-- Given a type, write the function

-- Example
myFunc :: (x -> y) -> (y -> z) -> c -> (a, x) -> (a, z)
myFunc xToY yToZ _ (a, x) = (a, yToZ $ xToY x)

-- 1
i :: a -> a
i a = a

-- 2
c :: a -> b -> a
c x2'' y2'' = x2''

-- 3
c'' :: b -> a -> b
c'' x3' y3' = x3'

-- 4
c' :: a -> b -> b
c' x4' y4' = y4'

-- 5
r :: [a] -> [a]
r (x5:xs5) = xs5

-- 6
co :: (b -> c) -> (a -> b) -> a -> c
co g f6 x6 = g $ f6 x6

-- 7
a :: (a -> c) -> a -> a
a _ x7 = x7

-- 8
a' :: (a -> b) -> a -> b
a' f8 = f8

-- Fix it

-- 1
-- module sing where
-- fstString :: [Char] ++ [Char]
-- fstString x = x ++ " in the rain"

-- sndString :: [Char] -> Char
-- sndString x = x ++ " over the rainbow"

-- sing = if (x > y) then fstString x or sndString y
-- where x = "Singin"
--       x = "Somewhere"

-- module Sing where
fstString :: [Char] -> [Char]
fstString xF1 = xF1 ++ " in the rain"

sndString :: [Char] -> [Char]
sndString xF1' = xF1' ++ " over the rainbow"

sing :: [Char]
sing = if (xF1'' > yF1) then fstString xF1'' else sndString yF1
  where xF1'' = "Singing"
        yF1 = "Somewhere"

-- 2
sing' :: [Char]
sing' = if (xF1'' < yF1) then fstString xF1'' else sndString yF1
  where xF1'' = "Singing"
        yF1 = "Somewhere"

-- 3
-- main :: IO ()
-- Main = do
--   print 1 + 2
--   putStrLn 10
--   print (negate -1)
--   print ((+) 0 blah)
--   where blah = negate 1

main :: IO ()
main = do
  print $ 1 + 2
  putStrLn "10"
  print (negate (-1))
  print ((+) 0 blah)
    where blah = negate 1


-- Type-Kwon-Do

-- Example
data Woot
data Blah

foo :: Woot -> Blah
foo = undefined

bar :: (Blah, Woot) -> (Blah, Blah)
bar (b, w) = (b, foo w)

-- 1
foo1 :: Int -> String
foo1 = undefined

bar1 :: String -> Char
bar1 = undefined

baz1 :: Int -> Char
baz1 n1 = bar1 $ foo1 n1

-- 2
data A
data B
data C

q :: A -> B
q = undefined

w :: B -> C
w = undefined

e :: A -> C
e n2 = w $ q n2

-- 3
data X
data Y
data Z

xz :: X -> Z
xz = undefined

yx :: Y -> Z
yx = undefined

xform :: (X, Y) -> (Z, Z)
xform (xi, yi) = (xz xi, yx yi)

-- 4
munge :: (x -> y) -> (y -> (w, z)) -> x -> w
munge xy ywz xii= fst $ ywz $ xy xii
