module Chapter5 where


-- Type Matching

-- 1. Functions
-- a) not => c
-- b) length => d
-- c) concat => b
-- d) head => a
-- e) (<) => e

-- 2. Type signatures
-- a) _ :: [a] -> a
-- b) _ :: [[a]] -> [a]
-- c) _ :: Bool -> Bool
-- d) _ :: [a] -> Int
-- e) _ :: Ord a => a -> a -> Bool


-- Type Arguments
-- 1
-- f :: a -> a -> -> a
-- x :: Char
-- :t f x => Char -> Char -> Char

-- 2
-- g :: a -> b -> c -> b
-- g 0 'c' "woot" => Char

-- 3
-- h :: (Num a, Num b) => a -> b -> b
-- h 1.0 2 => Num b => b

-- 4
-- h :: (Num a, Num b) => a -> b -> b
-- h 1 (5.5 :: Double) => Double

-- 5
-- jackal :: (Ord a, Eq b) => a -> b -> a
-- jackal "keyboard" "has the word jackal in it" => [Char]

-- 6
-- jackal :: (Ord a, Eq b) => a -> b -> a
-- jackal "keyboard" => Eq b => b -> [Char]

-- 7
-- kessel :: (Ord a, Num b) => a -> b -> a
-- kessel 1 2 => (Num a, Ord a) => a

-- 8
-- kessel :: (Ord a, Num b) => a -> b -> a
-- kessel 1 (2 :: Integer) => (Num a, Ord a) => a

-- 9
-- kessel :: (Ord a, Num b) => a -> b -> a
-- kessel (1 :: Integer) 2 => Integer


-- Exercises: Parametricity

-- 1
id' :: a -> a
id' a = a

-- 2
conc :: a -> a -> a
conc a _ = a

-- 3
foo :: a -> b -> b
foo _ b = b
-- 1 implementation. Behaviour doesn't change when types of a and b change.


-- Type Inference
f :: Num a => a -> a -> a
f x y = x + y + 3

f' x y = x + y + 3


-- Exercises: Apply Yourself
-- 1
-- (++) :: [a] -> [a] -> [a]
-- myConcat x = x ++ " yo"
-- myConcat :: [Char] -> [Char] -- because (++) is applied to a String ([Char])

-- 2
-- (*) :: Num a => a -> a -> a
-- myMult x = (x / 3) * 5
-- myMult :: Fractional a => a -> a -- bacause (/) requires a Fractional

-- 3
-- take :: Int -> [a] -> [a]
-- myTake x = take x "hey you"
-- myTake :: Int -> [Char]

-- 4
-- (>) :: Ord a => a -> a -> Bool
-- myCom x = x > (length [1..10])
-- myCom :: Int -> Bool

-- 5
-- (<) :: Ord a => a -> a -> Bool
-- myAlph x = x < 'z'
-- myAlph :: Char -> Bool
