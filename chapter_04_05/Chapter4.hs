module Chapter4 where


data Mood = Blah | Woot deriving Show

-- 1 => Mood
-- 2 => Blah or Woot
-- 3 => It should be Mood -> Mood
-- 4 =>
changeMood :: Mood -> Mood
changeMood Blah = Woot
changeMood _ = Blah


-- Exercises: Find the Mistakes

-- 1) not True && true => not (True && True)
-- 2) not (x = 6) => not (x == 6)
-- 3) (1 * 2) > 5 => ok
-- 4) [Merry] > [Happy] => ["Merry"] > ["Happy"]
-- 5) [1,2,3] ++ "look at me!" => ["1", "2", "3"] ++ ["look at me!"]


-- 4.6

greetIfCool :: String -> IO ()
greetIfCool coolness =
  if cool then
    putStrLn "eyyyyy. What's shaking'?"
  else
    putStrLn "pshhh."
  where
    cool =
      coolness == "downright frosty yo"


-- 4.7

tupFunc :: (Int, [a]) -> (Int, [a]) -> (Int, [a])
tupFunc (a,b) (c,d) =
  (a + c, b ++ d)
