module Chapter15 where


-- Exercises: Optional Monoid

data Optional a
  = Nada
  | Only a
  deriving (Eq, Show)

instance Monoid a => Monoid (Optional a) where
  mempty = Nada
  mappend (Only x) (Only y) = Only (mappend x y)
  mappend (Only x) Nada = Only x
  mappend Nada (Only x) = Only x
  mappend Nada Nada = Nada
