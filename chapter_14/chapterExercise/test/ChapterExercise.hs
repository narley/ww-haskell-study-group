{-# LANGUAGE FlexibleContexts #-}

import Test.QuickCheck
import Test.QuickCheck.Gen (oneof)
import Data.List (sort)
import Data.Char (toUpper, isAlpha)

-- 1
--for a function
half x = x / 2

-- this property should hold
halfIdentity = (*2) . half

genDivisor :: Gen Float
genDivisor = arbitrary `suchThat` (/= 0)

prop_half :: Property
prop_half =
  forAll genDivisor
  (\i -> halfIdentity i == i)


-- 2
-- for any list you apply sort to
-- this property should hold
listOrdered :: (Ord a) => [a] -> Bool
listOrdered xs =
  snd $ foldr go (Nothing, True) xs
  where
    go _ status@(_, False) = status
    go y (Nothing, t) = (Just y, t)
    go y (Just x, t) = (Just y, x >= y)

listGen :: Arbitrary a => Gen [a]
listGen = do
  xs <- arbitrary
  return xs

listIntGen :: Gen [Int]
listIntGen = listGen

listCharGen :: Gen [Char]
listCharGen = listGen

prop_sortIntList :: Property
prop_sortIntList =
  forAll listIntGen
  (\xs -> listOrdered (sort xs))

prop_sortCharList :: Property
prop_sortCharList =
  forAll listCharGen
  (\xs -> listOrdered (sort xs))


-- 3
plusAssociative x y z =
  x + (y + z) == (x + y) + z

plusCommutative x y =
  x + y == y + x

prop_plusAssociative :: Integer -> Integer -> Integer -> Bool
prop_plusAssociative = plusAssociative

prop_plusCommutative :: Integer -> Integer -> Bool
prop_plusCommutative = plusCommutative


-- 4
multiplicationAssociative x y z =
  x * (y * z) == (x * y) * z

multiplicationCommutative x y =
  x * y == y * x

genTuple :: (Arbitrary a, Eq a) => Gen (a, a)
genTuple = do
  a <- arbitrary
  b <- arbitrary `suchThat` (/= a)
  return (a, b)

genTriple :: (Arbitrary a, Eq a) => Gen (a, a, a)
genTriple = do
  a <- arbitrary
  b <- arbitrary `suchThat` (/= a)
  c <- arbitrary `suchThat` (`notElem` [a, b])
  return (a, b, c)

genIntTuple :: Gen (Int, Int)
genIntTuple = genTuple

genIntTriple :: Gen (Int, Int, Int)
genIntTriple = genTriple

prop_multiplicationAssociative :: Property
prop_multiplicationAssociative =
  forAll genIntTriple
  (\(x, y, z) -> multiplicationAssociative x y z)

prop_multiplicationCommutative :: Property
prop_multiplicationCommutative =
  forAll genIntTuple
  (\(x, y) -> multiplicationCommutative x y)


-- 5
-- quot rem
evalQuotRem x y =
  (quot x y) * y + (rem x y) == x

-- div mod
evalDivMod x y =
  (div x y) * y + (mod x y) == x

genDivTupleInteger :: Gen (Integer, Integer)
genDivTupleInteger = do
  a <- arbitrary
  b <- arbitrary `suchThat` (/=0)
  return (a, b)

prop_quotRem :: Property
prop_quotRem =
  forAll genDivTupleInteger (\(x, y) -> evalQuotRem x y)

prop_divMod :: Property
prop_divMod =
  forAll genDivTupleInteger (\(x, y) -> evalDivMod x y)


-- 6
caretAssociative x y z =
  (x ^ y) ^ z == y ^ (x ^ z)

caretCommutative x y =
  x ^ y == y ^ x

prop_caretAssociative :: Property
prop_caretAssociative =
  forAll genIntTriple (\(x, y, z) -> caretAssociative x y z)

prop_caretCommutative :: Property
prop_caretCommutative =
  forAll genIntTuple (\(x, y) -> caretCommutative x y)


-- 7
evalReverseList :: Eq a => [a] -> Bool
evalReverseList xs =
  (reverse . reverse) xs == (id xs)

prop_reverseIntList :: [Integer] -> Bool
prop_reverseIntList = evalReverseList

-- using listIntGen
prop_reverseIntList' :: Property
prop_reverseIntList' =
  forAll listIntGen evalReverseList

-- using listCharGen
prop_reverseCharList :: Property
prop_reverseCharList =
  forAll listCharGen evalReverseList


-- 8
prop_dollar :: (Show a, Eq b) => (a -> b) -> Gen a -> Property
prop_dollar f xGen =
  forAll xGen
  (\x -> (f $ x) == f x)

prop_compose :: (Show a, Eq c) => (b -> c) -> (a -> b) -> Gen a -> Property
prop_compose f g xGen =
  forAll xGen
  (\x -> f (g x) == (f . g) x)


-- 9
prop_foldCons :: Property
prop_foldCons =
  forAll genIntTriple
  (\(x, y, z) -> (foldr (:) [] [x, y, z]) == ([x] ++ [y] ++ [z]))

prop_foldConcat :: Property
prop_foldConcat =
  forAll genIntTriple
  (\(x, y, z) -> (foldr (++) [] [[x, y, z]]) == concat [[x, y, z]])


-- 10
genPosInt :: Gen Int
genPosInt = do
  a <- arbitrary `suchThat` (>= 0)
  return a

prop_ten :: Property
prop_ten =
  forAll genPosInt
  (\x -> (length (take x [1..])) == x)


-- 11
genInt :: Gen Int
genInt = arbitrary

prop_eleven :: Property
prop_eleven =
  forAll genInt
  (\x -> (read (show x)) == x)


-- Failure
square x = x * x

squareIdentity = square . sqrt

genFloat :: Gen Float
genFloat = arbitrary

prop_square :: Property
prop_square =
  forAll genFloat
  (\x -> collect x (squareIdentity x == x))


-- Idempotence
twice f = f . f

fourTimes = twice . twice

-- 1
capitalizeword :: String -> String
capitalizeword (x:xs) = toUpper x : xs

genWord :: Gen String
genWord = do
  w <- arbitrary `suchThat` (\w' -> (length w' > 0) && (isAlpha $ head w'))
  return w

evalIdempotence1 x =
  (capitalizeword x == twice capitalizeword x)
  &&
  (capitalizeword x == fourTimes capitalizeword x)

prop_idempotence1 :: Property
prop_idempotence1 =
  forAll genWord evalIdempotence1

-- 2
evalIdempotence2 x =
  (sort x == twice sort x) && (sort x == fourTimes sort x)

prop_idempotence2 :: Property
prop_idempotence2 =
  forAll listIntGen evalIdempotence2


-- Make a Gen random generator for the datatype
-- 1. Equal probabilities for each.
data Fool
  = Fulse
  | Frue
  deriving (Show, Eq)

genFool :: Gen Fool
genFool = elements [Fulse, Frue] -- elements :: [a] -> Gen a
-- genFool = oneof [return Fulse, return Frue] -- oneof :: [Gen a] -> Gen a

instance Arbitrary Fool where
  arbitrary = genFool

{-|
 The command below is the one to run on GHCI:
 > sample (arbitrary :: Gen Fool)

 and not:
 > sample Fool

 That's because 'sample Fool' will only generate samples
 based only on the 'elements [Fulse, Frue]'.

 In order to get a real sense of distribution we need to
 call 'arbitrary' with 'Gen Fool'.
-}

-- 2. 2/3s chance of Fulse, 1/3 change of Frue.
data Fool'
  = Fulse'
  | Frue'
  deriving (Show, Eq)

genFool' :: Gen Fool'
genFool' = elements [Fulse', Frue']

instance Arbitrary Fool' where
  arbitrary =
    frequency [ (2, return Fulse')
              , (1, return Frue')
              ]


-- Hangman testing
-- check hangman project (no idea what to test)


main :: IO ()
main = do
  quickCheck prop_half -- 1
  quickCheck prop_sortIntList -- 2
  quickCheck prop_sortCharList -- 2
  quickCheck prop_plusAssociative -- 3
  quickCheck prop_plusCommutative -- 3
  quickCheck prop_multiplicationAssociative -- 4
  quickCheck prop_multiplicationCommutative -- 4
  quickCheck prop_quotRem -- 5
  quickCheck prop_divMod -- 5
  quickCheck prop_caretAssociative -- 6
  quickCheck prop_caretCommutative -- 6
  quickCheck prop_reverseIntList -- 7
  quickCheck prop_reverseIntList' -- 7
  quickCheck prop_reverseCharList -- 7
  quickCheck (prop_dollar (take 1) listIntGen) -- 8
  quickCheck (prop_compose (take 1) reverse listCharGen) -- 8
  quickCheck prop_foldCons -- 9
  quickCheck prop_foldConcat -- 9
  quickCheck prop_ten -- 10
  quickCheck prop_eleven -- 11
  quickCheck prop_square -- Failure
  quickCheck prop_idempotence1 -- Idempotence 1
  quickCheck prop_idempotence2 -- Idempotence 2
