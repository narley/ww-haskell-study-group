module Addition where

import Test.Hspec
import Test.QuickCheck


dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
  where
    go n d count
      | n < d = (count, n)
      | otherwise = go (n - d) d (count + 1)


-- Intermission: Short Exercise

mult :: (Eq a, Num a, Ord a) => a -> a -> a
mult x y = go x y 0
  where
    go n m t
      | m == 0 = t
      | otherwise = go n (m-1) (t+n)


main :: IO ()
main = hspec $ do
  describe "Addition" $ do
    it "1 + 1 is greater than 1" $ do
      ((1 + 1) :: Int) > 1 `shouldBe` True
    it "2 + 2 is equal to 4" $ do
      (2 + 2 :: Int) `shouldBe` 4
    it "15 divided by 3 is 5" $ do
      dividedBy 15 3 `shouldBe` (5, 0)
    it "22 divided by 5 is 4 remainder 2" $ do
      dividedBy 22 5 `shouldBe` (4, 2)
    -- Intermission: Short Exercise
    it "2 mult 4 is 8" $ do
      mult (2 :: Int) (4 :: Int) `shouldBe` 8
    it "2 mult 0 is 0" $ do
      mult (2 :: Int) (0 :: Int) `shouldBe` 0
    it "2 mult 1 is 2" $ do
      mult (2 :: Int) (1 :: Int) `shouldBe` 2


-- 14.4 Enter QuickCheck
    it "x + 1 is always greater than x" $ do
      property $ \x -> x + 1 > (x :: Int)


-- trivial generator of values
trivialInt :: Gen Int
trivialInt = return 1


-- oneThroughThree :: Gen Int
oneThroughTrhee = elements [1, 2, 3]


genBool :: Gen Bool
genBool = choose (False, True)

genBool' :: Gen Bool
genBool' = elements [False, True]

genOrdering :: Gen Ordering
genOrdering = elements [LT, EQ, GT]

genChar :: Gen Char
genChar = elements ['a'..'z']

genTuple :: (Arbitrary a, Arbitrary b) => Gen (a, b)
genTuple = do
  a <- arbitrary
  b <- arbitrary
  return (a, b)

genThreeple :: (Arbitrary a, Arbitrary b, Arbitrary c) => Gen (a, b, c)
genThreeple = do
  a <- arbitrary
  b <- arbitrary
  c <- arbitrary
  return (a, b, c)

genEither :: (Arbitrary a, Arbitrary b) => Gen (Either a b)
genEither = do
  a <- arbitrary
  b <- arbitrary
  elements [Left a, Right b]

-- equal probability
genMaybe :: Arbitrary a => Gen (Maybe a)
genMaybe = do
  a <- arbitrary
  elements [Nothing, Just a]

-- what QuickCheck does so you get more Just values
-- frequency :: [(Int, Gen a)] -> Gen a
genMaybe' :: Arbitrary a => Gen (Maybe a)
genMaybe' = do
  a <- arbitrary
  frequency [ (1, return Nothing)
            , (3, return (Just a))
            ]

-- using QuickCheck without Hspec
prop_additionGreater :: Int -> Bool
prop_additionGreater x = x + 1 > x

runQc :: IO ()
runQc = quickCheck prop_additionGreater


-- 14.5 Morse code
-- check new project...


-- 14.6 Arbitrary instances

-- Babby's First Arbitrary
-- check BabbyFirstArbitrary folder

-- Identity Crisis
-- check IdentityCrisis folder

-- Arbitrary Products
-- check ArbitraryProducts folder

-- Greater than the sum of its parts
-- check GreaterThanSum folder
