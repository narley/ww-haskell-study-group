module NumberIntoWord
    ( digitToWord
    , digits
    , wordNumber
    ) where

import Data.List (intercalate)

-- Numbers into words

digitToWord :: Int -> String
digitToWord n
  | n == 1 = "one"
  | n == 2 = "two"
  | n == 3 = "three"
  | n == 4 = "four"
  | n == 5 = "five"
  | n == 6 = "six"
  | n == 7 = "seven"
  | n == 8 = "eight"
  | n == 9 = "nine"
  | otherwise = "zero"

digits :: Int -> [Int]
digits n = go n []
  where
    go x xs
      | div x 10 == 0 = x : xs
      | otherwise = go (div x 10) (mod x 10 : xs)

wordNumber :: Int -> String
wordNumber n =
  intercalate "-" $ map digitToWord $ digits n
