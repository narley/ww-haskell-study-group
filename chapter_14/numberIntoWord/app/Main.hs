module Main where

import NumberIntoWord
import System.IO

main :: IO ()
main = do
  number <- readLn
  putStrLn $ wordNumber number
