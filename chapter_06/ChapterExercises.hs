module ChapterExercises where

import Data.List

-- Multiple choice

-- 1 => c
-- 2 => a, b
-- 3 => a
-- 4 => d
-- 5 => a

-- Does it typecheck?

-- 1
-- data Person = Person Bool
-- printPerson :: Person -> IO ()
-- printPerson person = putStrLn (show person)
-- Doesn't work because Person has no instance of Show.

-- 2
-- data Mood = Blah | Woot deriving (Eq, Show)
-- settleDown x = if x == Woot then Blah else x
-- Doesn't work because 'Mood' has no instance of Eq.

-- 3
-- a) Woot or Blah.
-- b) Would get a type error because 9 and 'Woot' are not of the same type.
-- c) Doesn't work because 'Mood' has no instance of Ord.

-- 4
-- type Subject = String
-- type Verb = String
-- type Object = String
-- data Sentence =
--   Sentence Subject Verb Object
--   deriving (Eq, Show)
-- s1 = Sentence "dogs" "drool"
-- s2 = Sentence "Julie" "loves" "dogs"
-- s1 doesn't work because it will return a function that requires a Object which has no instance of Show.
-- s2 works because it returns a result which can be printed on the screen because it has an instance of Show.

-- Given a datatype declaration, what can we do?

data Rocks = Rocks String deriving (Eq, Show)
data Yeah = Yeah Bool deriving (Eq, Show)
data Papu = Papu Rocks Yeah deriving (Eq, Show)

-- 1
-- phew = Papu "chases" True
-- won't typecheck because "chases" is not of type Rocks and 'True' is not of type Yeah.

-- 2
-- truth = Papu (Rocks "chomskydoz") (Yeah True)
-- will typecheck

-- 3
-- equalityForall :: Papu -> Papu -> Bool
-- equalityForall p p' = p == p'
-- will typecheck

-- 4
-- comparePapus :: Papu -> Papu -> Bool
-- comparePapus p p' = p > p'
-- won't typecheck because Papu, Yeah and Rocks don't have instance of Ord

-- Match the types

-- 1
-- a)
-- i :: Num a => a
-- i = 1
-- b)
-- i :: a
-- 'b' won't compile because 'a' doesn't match the type of 1.

-- 2
-- a)
-- f :: Float
-- f = 1.0
-- b)
-- f :: Num a => a
-- 'b' won't compile because 'Float' is a primitive type.

-- 3
-- a)
-- f :: Float
-- f = 1.0
-- b)
-- f :: Fractional a => a
-- 'b' will compile because 'Fractional' is a type class that has 'Num' as a constrained polymorphic type.

-- 4
-- a)
-- f :: Float
-- f = 1.0
-- b)
-- f :: RealFrac a => a
-- 'b' will compile because 'RealFrac' is a type class that has 'Fractional' (this one has 'Num' as constrained polymorphic type) as a constrained polymorphic type.

-- 5
-- a)
-- freud :: a -> a
-- freud x = x
-- b)
-- freud :: Ord a => a -> a
-- 'b' will compile because the function term in 'freud' is not specifying what type is going to take, so the type class 'Ord' is a good candidate.

-- 6
-- a)
-- freud' :: a -> a
-- freud' x = x
-- b)
-- freud' :: Int -> Int
-- 'b' will compile because the term in 'freud'' is not specifying what type is going to take, so parametric polymorphism with 'Int' is a good candidate.

-- 7
-- a)
-- myX = 1 :: Int
-- sigmund :: Int -> Int
-- sigmund x = myX
-- b)
-- sigmund :: a -> a
-- 'b' won't compile because 'sigmund' can't match type 'a' with 'Int'. We can't generalise a function that is already specialised (by applying to 'myX' which is an 'Int').

-- 8
-- a)
-- myX = 1 :: Int
-- sigmund' :: Int -> Int
-- sigmund' x = myX
-- b)
-- sigmund' :: Num a => a -> a
-- 'b' won't compile because 'sigmund'' can't match type 'a' with 'Int' even though it's using constrained polymorphic 'Num a'. We can't generalise a function that is already specialised (by applying to 'myX' which is and 'Int').

-- 9
-- a)
-- jung :: Ord a => [a] -> a
-- jung xs = head (sort xs)
-- b)
-- jung :: [Int] -> Int
-- 'b' will compile because it is specialising a more general implementation by using parametric polymorphism.

-- 10
-- a)
-- young :: [Char] -> Char
-- young xs = head (sort xs)
-- b)
-- young :: Ord a => [a] -> a
-- 'b' will compile because even though it's been generalised, 'sort' expects its argument to be instance of type class 'Ord'.

-- 11
-- a)
-- mySort :: [Char] -> [Char]
-- mySort = sort
-- signifier :: [Char] -> Char
-- signifier xs = head (mySort xs)
-- b)
-- signifier :: Ord a => [a] -> a
-- 'b' won't compile because 'mySort' is a specialised function (has concrete types) and 'signifier' is a constrained polymorphic function (trying to generalise).


-- Type-Kwond-Do Two: Electric Typealoo

-- 1
chk :: Eq b => (a -> b) -> a -> b -> Bool
chk f x y = f x == y

-- 2
arith :: Num b => (a -> b) -> Integer -> a -> b
arith f x y = f y + (fromInteger x)
