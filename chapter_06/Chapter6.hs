module Chapter6 where


{-
Eq -> Ord -> Enum
To be able to put something in an enumerated list,
they must be ordered; to be able to order something,
they must be able to be compared for equality.
Eq is a superclass of Ord
Ord is a superclass of Enum
Therefore Eq is a superclass for Enum
-}

data Trivial =
  Trivial'

instance Eq Trivial where
  Trivial' == Trivial' = True
  Trivial' /= Trivial' = False


data DayOfWeek
  = Mon
  | Tue
  | Weds
  | Thu
  | Fri
  | Sat
  | Sun
  deriving Show

instance Eq DayOfWeek where
  (==) Mon Mon = True
  (==) Tue Tue = True
  (==) Weds Weds = True
  (==) Thu Thu = True
  (==) Fri Fri = True
  (==) Sat Sat = True
  (==) Sun Sun = True
  (==) _ _ = False


data Date =
  Date DayOfWeek Int
  deriving Show

instance Eq Date where
  (==) (Date weekDay dayOfMonth) (Date weekDay' dayOfMonth') =
    weekDay == weekDay' && dayOfMonth == dayOfMonth'


f :: Int -> Bool
f 2 = True
f _ = False


data Identity a =
  Identity a

instance Eq a => Eq (Identity a) where
  (==) (Identity v) (Identity v') = v == v'


data NoEq = NoEquInst deriving Show


-- 6.5 => Exercises Eq Instances

-- 1
data TisAnInteger =
  TisAn Integer

instance Eq TisAnInteger where
  (==) (TisAn 1) (TisAn 1) = True
  (==) _ _ = False

-- 2
data TwoIntegers =
  Two Integer Integer

instance Eq TwoIntegers where
  (==) (Two 10 10) (Two 10 10) = True
  (==) _ _ = False

-- 3
data StringOrInt
  = TisAnInt Int
  | TisAString String

instance Eq StringOrInt where
  (==) (TisAnInt 1) (TisAnInt 1) = True
  (==) (TisAString "foo") (TisAString "foo") = True
  (==) _ _ = False

-- 4
data Pair a =
  Pair a a

instance Eq a => Eq (Pair a) where
  (==) (Pair x y) (Pair x' y') =
    x == x' && y == y'

-- 5
data Tuple a b =
  Tuple a b

instance (Eq a, Eq b) => Eq (Tuple a b) where
  (==) (Tuple x y) (Tuple x' y') =
    x == x' && y == y'

-- 6
data Which a
  = ThisOne a
  | ThatOne a

instance Eq a => Eq (Which a) where
  (==) (ThisOne v) (ThisOne v') = v == v'
  (==) (ThatOne v) (ThatOne v') = v == v'
  (==) _ _ = False

-- 7
data EitherOr a b
  = Hello a
  | Goodbye b

instance (Eq a, Eq b) => Eq (EitherOr a b) where
  (==) (Hello a) (Hello a') = a == a'
  (==) (Goodbye b) (Goodbye b') = b == b'
  (==) _ _ = False


-- 6.8 -> Ord Instances
instance Ord DayOfWeek where
  compare Fri Fri = EQ
  compare Fri _ = GT
  compare _ Fri = LT
  compare _ _ = EQ


-- Exercise: Will They Work?

-- 1
-- max (length [1,2,3]) (length [8,9,10,11,12])
-- Works. 5. Works because length returns Int which has an instance of Ord which is required by 'max'.

-- 2
-- compare (3 * 4) (3 * 5)
-- Works. LT. Works because (*) returns Num which has an instance which has an instance of Int which has an instance of Ord which is required by 'compare'.

-- 3
-- compare "Julie" True
-- Doesn't work because 'compare' requires its parameters to be of type Ord but True is not of the same type as "Julie".

-- 4
-- (5+3) > (3+6)
-- Works. False. Works because (+) returns Num which has instance of Int which has an instance of Ord which is required by '>'.


-- 6.10

data Mood = Blah

instance Show Mood where
  show _ = "Blah"


-- 6.12

class Numberish a where
  fromNumber :: Integer -> a
  toNumber :: a -> Integer

newtype Age =
  Age Integer
  deriving (Eq, Show)

instance Numberish Age where
  fromNumber n = Age n
  toNumber (Age n) = n

newtype Year =
  Year Integer
  deriving (Eq, Show)

instance Numberish Year where
  fromNumber n = Year n
  toNumber (Year n) = n

sumNumberish :: Numberish a => a -> a -> a
sumNumberish a b = fromNumber summed
  where
    integerOfA = toNumber a
    integerOfB = toNumber b
    summed = integerOfA + integerOfB
