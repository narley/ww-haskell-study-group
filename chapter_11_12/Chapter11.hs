{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
module Chapter11 where

import Data.Int


-- 11.5 Data constructor and values

data PugType = PugData

{-|
  PugType: is the type constructor, but it takes no arguments so we can think of it as being a type constant.
  This is how the Haskell Report refers to such types.
  This type enumerates one constructor.

  PugData: is the only data constructor for the type PugType.
  It also happens to be a constant value because it takes no arguments and stands only for itself.
  For any function that requires a value of type PugType, you know that value will be PugData.
-}


data HuskyType a = HuskyData

{-|
  HuskyType: is the type constructor, and it take a single parametrically polymorphic type variable as an argument.
  It also enumerates one data constructor.

  HuskyData: is the data constructor for HuskyType.
  Note that the type variable argument 'a' does not occur as an argument to HuskyData or anywhere else after the '='.
  That means our type argument 'a' is 'phantom', or, "has no witness".
  We will elaborate on this later. Here HuskyData is a constant value, like PugData.
-}


data DogueDeBordeaux doge = DogueDeBordeaux doge

{-|
  DogueDeBordeaux: is a type constructor and has a single type variable argument like HuskyType, but called 'doge' instead of 'a'.
  Why? Becuase the names of variables don't matter.
  At any rate, this type also enumerates one constructor.

  DogueDeBordeaux: is the lone data constructor.
  It has the same name as the type constructor, but they are not the same thing.
  The doge type variable in the type constructor occurs also in the data constructor.
  Remember that, because they are the same type variable, these must agree with each other: 'doge' must equal 'doge'.
  If your type is DogueDeBordeaux [Person], you must necessarily have a list of Person values contained in the DogueDeBordeaux value.
  But because DogueDeBordeaux must be applied before it's a concrete value, its literal value at runtime can change.
-}


data Doggies a
  = Husky a
  | Mastiff a
  deriving (Eq, Show)


-- Exercises: Dog Types
{-|
1)
type constructor

2)
Doggies :: * -> *

3)
Doggies String :: *

4)
Husky 10 :: Num a => Doggies a

5)
Husky (10 :: Integer) :: Doggies Integer

6)
Mastiff "Scooby Doo" :: Doggies String

7)
Data constructor

8)
DogueDeBordeaux :: doge -> DogueDeBordeaux doge

9)
DogueDeBordeaux "doggie!" :: DogueDeBordeaux String
-}


-- 11.6 What's a type and what's data?

{-|
  type constructor --> compile-time

  -------------------- phase separation

  data constructor --> runtime
-}

data Price = Price Integer deriving (Eq, Show)
--    (a)     (b)    [1]

-- (a): type constructor
-- (b): data constructor
-- [1]: type argument

data Manufacturer
--   type constructor
  = Mini
--  data constructor
  | Mazda
--  data constructor
  | Tata
--  data constructor
  deriving (Eq, Show)

data Airline
--   type constructor
  = PapuAir
--  data constructor
  | CatapultsR'Us
--  data constructor
  | TakeYourChangesUnited
--  data constructor
  deriving (Eq, Show)

data Vehicle
--   type constructor
  = Car Manufacturer Price
--  data constructor / type argument / type argument
  | Plane Airline Size
--  data constructor / type argument
  deriving (Eq, Show)

data Size = Size Integer deriving (Eq, Show)


-- Exercises: Vehicles

myCar = Car Mini  (Price 14000)
urCar = Car Mazda (Price 20000)
clownCar = Car Tata (Price 7000)
doge = Plane PapuAir (Size 20)

-- 1
-- myCar :: Vehicle

-- 2
isCar :: Vehicle -> Bool
isCar v =
  case v of
    (Car _ _) -> True
    _ -> False

isPlane :: Vehicle -> Bool
isPlane v =
  case v of
    (Plane _ _) -> True
    _ -> False

areCars :: [Vehicle] -> [Bool]
areCars = map isCar


-- 3
getManu :: Vehicle -> Manufacturer
getManu (Car manu _) =
    case manu of
      Mini -> Mini
      Mazda -> Mazda
      Tata -> Tata

-- 4
-- It will cause an exception due to non-exhaustive patterns (Plane)

-- 5
-- done



-- 11.7 Data constructor arities

data MyType = MyVal Int deriving (Eq, Show)
--   type constructor / data constructor / type argument


-- 11.8 What makes these datatypes algebraic?

{-|
  Finite sets contain a number of unique objects; that number is called cardinality.

  The cardinality of a datatype is the number of possible values it defines.

  Knowing how many possible values inhabit a type can help reason about programs, for example determine how many different possible implementations there are of a function for a given type signature.

  Int8 range is from -128 to 127 including 0. We can easily figure out the cardinality with quick addition: 128 + 127 + 1 = 256.
  So there are 256 possible runtime values when using Int8.
-}

-- Exercises: Cardinality
{-|
1)
data PugType = PugData
cardinality is 1

2)
data Airline
  = PapuAir
  | CatapultsR'Us
  | TakeYourChangesUnited
cardinality is 3

3)
cardinality for Int16 is 65536

4)
There is no instance for Bounded Integer

5)
Int8 = 2^8 = 256
-}


-- Simple datatypes with nullary data constructors

data Example = MakeExample deriving Show


-- Exercises: For Example

-- 1a
-- MakeExample :: Example
-- 1b
-- Error is given because Example is not a Data Constructor.

-- 2
-- The only type class instance available is Show.

-- 3
data ExampleTwo = MakeExampleTwo Int deriving Show
-- MakeExampleTwo 10 :: ExampleTwo


-- 11.9 newtype

{-|
  'newtype' key-word mark a type that can only ever have a single unary data constructor.

  'newtype' can't be a product type, sum type, or contain nullary constructors.

  'newtype' has few advantages like not having runtime overhead. That's because it reuses the representation of the type it contains. It can do this because it's not allowed to be a record (product type) or tagged union (sum type).

  The difference between 'newtype' and the type it contains is gone by the time the compiler generates the code.
-}

newtype Goats = Goats Int deriving (Eq, Show, TooMany)

newtype Cows = Cows Int deriving (Eq, Show)

tooManyGoats :: Goats -> Bool
tooManyGoats (Goats n) = n > 42

class TooMany a where
  tooMany :: a -> Bool

instance TooMany Int where
  tooMany n = n > 42

-- This is commented because we are using
-- GeneralizedNewtypeDeriving pragma
-- instance TooMany Goats where
--   tooMany (Goats n) = tooMany n


{-|
  A pragma is a special instruction to the compiler palced in source code.
  The LANGUAGE pragma is perhaps more common in GHC Haskell than the other pragmas, but there are other pragmas.

  GeneralizedNewtypeDeriving: LANGUAGE pragma that tells the compiler to allow newtype to rely on a type class instance for the type it contains.
-}


-- Exercises: Logic Goats

-- 1
instance TooMany (Int, String) where
  tooMany (n, s) = n > 42 && s == "foo"

-- 2
instance TooMany (Int, Int) where
  tooMany (n, m) = tooMany (n + m)

-- 3
-- exercise 2 needs to be commented in order to test this one.
instance (Num a, TooMany a) => TooMany (a, a) where
  tooMany (n, m) = tooMany (n + m)



-- 11.10 Sum types

{-|
  "|": represents logical disjunction ("or"). This is the "sum" in algeraic datatypes.

  To know the cardinality of sum types, we add the cardinalities of their data constructors. True and False take no type arguments (nullary constructors). Nullary constructors have value of 1.
-}


-- Exercises: Pity the Bool

-- 1
data BigSmall
  = Big Bool
  | Small Bool
  deriving (Eq, Show)

{-|
  Big (True|False)
  Big (1 + 1) = 2
  Small (True|False)
  Small (1 + 1) = 2
  2 + 2
  4
-}

-- 2

data NumberOrBool
  = Numba Int8
  | BoolyBool Bool
  deriving (Eq, Show)

-- let myNumba = Numba (-128)

-- NumberOrBool cardinality: 258
-- It will give an error because the numbers are out of boundery.



-- 11.11 Product types

{-|
  Product type's cardinality is the product of the cardinalities of its inhabitants.
  Product type expresses "and".
  Product is a way to carry multiple values around in a single data constructor.
  Any data constructor with two or more type arguments is a product.
  Tuples are anonymous products: ( , ) :: a -> b -> (a,b).
-}

data QuantumBool
  = QuantumTrue
  | QuantumFalse
  | QuantumBoth
  deriving (Eq, Show)

data TwoQs
  = MkTwoQs QuantumBool QuantumBool
  deriving (Eq, Show)

{-|
  TwoQs could have been written using type alias and the tuple data constructor.
  Type aliases create type constructors, not data constructors.
  type TwoQs = (QuantumBool, QuantumBool)
-}

-- Record syntax
{-|
  Records are product types which provide accessors to fields withing the record.
-}

data Person
  = MkPerson String Int
  deriving (Eq, Show)

jm = MkPerson "julie" 108
ca = MkPerson "chris" 16

name :: Person -> String
name (MkPerson n _) = n

age :: Person -> Int
age (MkPerson _ a) = a

data Person' = Person'
  { name' :: String
  , age' :: Int
  }
  deriving (Eq, Show)

jm' = Person' "julie" 108
ca' = Person' "chris" 16



-- 11.12 Normal form

{-|
  data Fiction = Fiction deriving Show
  data NonFiction = NonFiction deriving Show

  data BookType
    = FictionBook Fiction
    | NonFictionBook NonFiction
    deriving Show

  It's important to remember that although the type constructors and data constructors of Fiction and NonFiction have the same name they are not the same, and it is the TYPE CONSTRUCTORS that are the arguments to FictionBook and NoFictionBook.
-}

type AuthorName = String

-- Not in normal form because it's not a sum of products: a * (b + c).
-- data Author = Author (AuthorName, BookType) deriving (Show)

-- In normal form because it's a sum of products: (a * b) + (a * c).
data Author
  = Fiction AuthorName
  | NonFiction AuthorName
  deriving (Eq, Show)

{-|
  Normal form == sum of products
-}


-- Exercises: How Does Your Garden Grow?

-- 1
data FlowerType
  = Gardenia
  | Daisy
  | Rose
  | Lilac
  deriving Show

type Gardener = String

data Garden
  = Garden Gardener FlowerType
  deriving Show

data Garden'
  = Gardenia' Gardener
  | Daisy' Gardener
  | Rose' Gardener
  | Lilac' Gardener
  deriving Show



-- 11.13 Constructing and deconstructing values

data GuessWhat = Chickenbutt deriving (Eq, Show)

data Id a = MkId a deriving (Eq, Show)

data Product a b = Product a b deriving (Eq, Show)

data Sum a b
  = First a
  | Second b
  deriving (Eq, Show)

data RecordProduct a b = RecordProduct
  { pfirst :: a
  , psecond :: b
  }
  deriving (Eq, Show)


-- Sum and Product

newtype NumCow = NumCow Int deriving (Eq, Show)

newtype NumPig = NumPig Int deriving (Eq, Show)

data Farmhouse = Farmhouse NumCow NumPig deriving (Eq, Show)

type Farmhouse' = Product NumCow NumPig

-- Farmhouse and Farmhouse' are the same.

newtype NumSheep = NumSheep Int deriving (Eq, Show)

data BigFarmhouse = BigFarmhouse NumCow NumPig NumSheep deriving (Eq,Show)

type BigFarmhouse' = Product NumCow (Product NumPig NumSheep)

-- BigFarmhouse and BigFarmhouse' are the same.

type Name = String
type Age = Int
type LovesMud = Bool
type PoundsOfWool = Int

data CowInfo = CowInfo Name Age deriving (Eq, Show)
data PigInfo = PigInfo Name Age LovesMud deriving (Eq, Show)
data SheepInfo = SheepInfo Name Age PoundsOfWool deriving (Eq, Show)

data Animal
  = Cow CowInfo
  | Pig PigInfo
  | Sheep SheepInfo
  deriving (Eq, Show)

-- or
type Animal' = Sum CowInfo (Sum PigInfo SheepInfo)


-- Constructing values

triviaValue :: GuessWhat
triviaValue = Chickenbutt

idInt :: Id Integer
idInt = MkId 10

type Awesome = Bool

person :: Product Name Awesome
person = Product "Simon" True

data Twitter = Twitter deriving (Eq, Show)
data AskFm = AskFm deriving (Eq, Show)

{-|
  Sum Twitter AskFm tells which goes with the data constructor "First" and which goes with the data constructor "Second".
-}
socialNetwork :: Sum Twitter AskFm
socialNetwork = First Twitter

{-|
  Asserting the ordering directly by writing a datatype like the following:
-}
data SocialNetwork
  = Twitter'
  | AskFm'
  deriving (Eq, Show)

{-|
  Using type alias can be dangerous because, for example, in "askFm", the function implies we meant Second "AskFm", but it was mistakenly defined with First "AskFm".
  That's because Twitter'' and AskFM'' are both string.
  Try to avoid type alias with unstructured data like test or binary.
-}
type Twitter'' = String
type AskFm'' = String

twitter :: Sum Twitter'' AskFm''
twitter = First "Twitter"

askFm :: Sum Twitter'' AskFm''
askFm = First "AskFm"


myRecord :: RecordProduct Integer Float
myRecord = RecordProduct 42 0.000001

myRecord' :: RecordProduct Integer Float
myRecord' = RecordProduct { pfirst = 42, psecond = 0.00001 }

data OperatingSystem
  = GnuPlusLinux
  | OpenBSDPlusNevermindJustBSDStill
  | Mac
  | Windows
  deriving (Eq, Show)

data ProgLang
  = Haskell
  | Agda
  | Idris
  | PureScript
  deriving (Eq, Show)

data Programmer =
  Programmer
  { os :: OperatingSystem
  , lang :: ProgLang
  }
  deriving (Eq, Show)

nineToFive :: Programmer
nineToFive = Programmer { os = Mac, lang = Haskell }

{-| We can reorder stuff when we use record syntax -}
feelingWizardly :: Programmer
feelingWizardly = Programmer { lang = Agda, os = GnuPlusLinux }


-- Exercise: Programmers
allOperatingSystems :: [OperatingSystem]
allOperatingSystems =
  [ GnuPlusLinux
  , OpenBSDPlusNevermindJustBSDStill
  , Mac
  , Windows
  ]

allLanguages :: [ProgLang]
allLanguages = [Haskell, Agda, Idris, PureScript]

allProgrammers :: [Programmer]
allProgrammers = [Programmer os lang | os <- allOperatingSystems, lang <- allLanguages]

{-| Partial application of data constructor -}
data ThereYet = There Float Int Bool deriving (Eq, Show)

nope = There

notYet :: Int -> Bool -> ThereYet
notYet = nope 25.5

notQuite :: Bool -> ThereYet
notQuite = notYet 10

yess :: ThereYet
yess = notQuite False

{-|
  The way the types progressed
  There    :: Float -> Int -> Bool -> ThereYet
  notYet   ::          Int -> Bool -> ThereYet
  notQuite ::                 Bool -> ThereYet
  yess     ::                         ThereYet
-}


-- Deconstructing values

newtype Name' = Name' String deriving Show
newtype Acres = Acres Int deriving Show

{-| FarmerType is a Sum-}
data FarmerType
  = DairyFarmer
  | WheatFarmer
  | SoybeanFarmer
    deriving Show

{-| Farmer is a plain old product of Name, Acres and FarmerTyper -}
data Farmer = Farmer Name Acres FarmerType deriving Show

isDairyFarmer :: Farmer -> Bool
isDairyFarmer (Farmer _ _ DairyFarmer) = True
isDairyFarmer _ = False

{-| Deconstructing records -}
data FarmerRec =
  FarmerRec
  { nameFR :: Name
  , acres :: Acres
  , farmerType :: FarmerType
  }
  deriving Show

isDairyFarmerRec :: FarmerRec -> Bool
isDairyFarmerRec farmer =
  case farmerType farmer of
    DairyFarmer -> True
    _ -> False


-- Accidental bottoms from records
{-| DO NOT DO THIS!!!!!!! -}
data Automobile
  = Null
  | Car' { make :: String
        , model :: String
        , year :: Integer
        }
  deriving (Eq, Show)

{-|
  How to fix this?
  First, whenever we have a product that uses record accessors, keep it separate of any sum type that is wrapping it.
  To do this, split out the product into an independent type with its own type constructor instead of only as an inline data constructor product:
-}

{-| Split out the record/product -}
data Car'' =
  Car''
  { make' :: String
  , model' :: String
  , year' :: Integer
  }
  deriving (Eq, Show)

{-| The Null is still not great, but we're leaving it in to make a point -}
data Automobile'
  = Null'
  | Automobile' Car''
  deriving (Eq, Show)


-- 11.14 Function type is exponential
{-|
  Given a function a -> b, we can calculate the inhabitants with the formula "b^a".
  So if b and a are Bool, then 2^2 is how we could express the number of inhabitants in a function of Bool -> Bool.
  A function of Bool to something of 3 inhabitants would be 3^2 and thus have nine possible implementations.
  a -> b -> c
  (c ^ b) ^ a
  c ^ (b * a)
-}

data Quantum
  = Yes
  | No
  | Both
  deriving (Eq, Show)

{-| 3 + 3 -}
quantSum1 :: Either Quantum Quantum
quantSum1 = Right Yes

quantSum2 :: Either Quantum Quantum
quantSum2 = Right No

quantSum3 :: Either Quantum Quantum
quantSum3 = Right Both

quantSum4 :: Either Quantum Quantum
quantSum4 = Left Yes

quantSum5 :: Either Quantum Quantum
quantSum5 = Left No

quantSum6 :: Either Quantum Quantum
quantSum6 = Left Both

{-| 3 * 3 -}
quantProd1 :: (Quantum, Quantum)
quantProd1 = (Yes, Yes)

quantProd2 :: (Quantum, Quantum)
quantProd2 = (Yes, No)

quantProd3 :: (Quantum, Quantum)
quantProd3 = (Yes, Both)

quantProd4 :: (Quantum, Quantum)
quantProd4 = (No, Yes)

quantProd5 :: (Quantum, Quantum)
quantProd5 = (No, No)

quantProd6 :: (Quantum, Quantum)
quantProd6 = (No, Both)

quantProd7 :: (Quantum, Quantum)
quantProd7 = (Both, Yes)

quantProd8 :: (Quantum, Quantum)
quantProd8 = (Both, No)

quantProd9 :: (Quantum, Quantum)
quantProd9 = (Both, Both)

{-| 3 ^ 3 -}
quantFlip1 :: Quantum -> Quantum
quantFlip1 Yes = Yes
quantFlip1 No = Yes
quantFlip1 Both = Yes

quantFlip2 :: Quantum -> Quantum
quantFlip2 Yes = No
quantFlip2 No = No
quantFlip2 Both = No

quantFlip3 :: Quantum -> Quantum
quantFlip3 Yes = Both
quantFlip3 No = Both
quantFlip3 Both = Both
{-| etc ... -}


-- Exponentiation in what order?
convert1 :: Quantum -> Bool
convert1 Yes = True
convert1 No = True
convert1 Both = True

convert2 :: Quantum -> Bool
convert2 Yes = True
convert2 No = True
convert2 Both = False

convert3 :: Quantum -> Bool
convert3 Yes = True
convert3 No = False
convert3 Both = True

convert4 :: Quantum -> Bool
convert4 Yes = True
convert4 No = False
convert4 Both = False

convert5 :: Quantum -> Bool
convert5 Yes = False
convert5 No = True
convert5 Both = True

convert6 :: Quantum -> Bool
convert6 Yes = False
convert6 No = True
convert6 Both = False

convert7 :: Quantum -> Bool
convert7 Yes = False
convert7 No = False
convert7 Both = True

convert8 :: Quantum -> Bool
convert8 Yes = False
convert8 No = False
convert8 Both = False


-- Exercises: The Quad

-- 1
data Quad
  = One
  | Two
  | Three
  | Four
  deriving (Eq, Show)

eQuad1 :: Either Quad Quad
eQuad1 = Right One

eQuad2 :: Either Quad Quad
eQuad2 = Right Two

eQuad3 :: Either Quad Quad
eQuad3 = Right Three

eQuad4 :: Either Quad Quad
eQuad4 = Right Four

eQuad5 :: Either Quad Quad
eQuad5 = Left One

eQuad6 :: Either Quad Quad
eQuad6 = Left Two

eQuad7 :: Either Quad Quad
eQuad7 = Left Three

eQuad8 :: Either Quad Quad
eQuad8 = Left Four

-- 2
prodQuad :: (Quad, Quad)
prodQuad = undefined -- 4 * 4 = 4

-- 3
funcQuad :: Quad -> Quad
funcQuad = undefined -- 4^4 = 256

-- 4
prodTBool :: (Bool, Bool, Bool)
prodTBool = undefined -- 2 * 2 * 2 = 8

-- 5
gTwo :: Bool -> Bool -> Bool
gTwo = undefined -- a -> b -> c = (c ^ b) ^ a = (2 ^ 2) ^ 2 = 16
{-|
T T = T
F T = T

T T = F
F T = F

T F = T
F F = T

T F = F
F F = F

T T = T
F F = T

T T = F
F F = F

T T = T
F F = F

T T = F
F F = T

-------

F T = T
T T = T

F T = F
T T = F

F F = T
T F = T

F F = F
T F = F

F T = T
T F = T

F T = F
T F = F

F T = T
T F = F

F T = F
T F = T
-}

-- 6
fTwo :: Bool -> Quad -> Quad
fTwo = undefined -- a -> b -> c = (c ^ b) ^ a = (4 ^ 4) ^ 2 = 65532



-- 11.15 Higher-kinded datatypes

{-|
  Kinds are the types of type constructors.
  Default kind in Haskell is '*'.
  The kind * -> * is waiting for a single * before it is fully applied.
  The kind * -> * -> * must be applied twice before it will be a real type.
  This kind application is known as a higher-kinded type.
  List are higher-kinded datatypes in Haskell.
-}

{-| identical to (a,b,c,d) -}
data Silly a b c d = MkSilly a b c d deriving Show

{-|
  :kind Silly
  Silly :: * -> * -> * -> * -> *

  :kind Silly Int
  Silly :: * -> * -> * -> *

  :kind Silly Int String
  Silly :: * -> * -> *

  :kind Silly Int String Bool
  Silly :: * -> *

  :kind Silly Int String Bool String
  Silly :: *

  -- Identical to (a,b,c,d)
  :kind (,,,)
  (,,,) :: * -> * -> * -> * -> *

  :kind (Int,String,Bool,String)
  (Int,String,Bool,String) :: *
-}

{-|
data EsResultFound a =
  EsResultFound
  { _version :: DocVersion
  , _source :: a
  } deriving (Eq, Show)

instance (FromJSON a) => FromJSON (EsResultFound a) where
  parseJSON (Object v) = EsResultFound
    <$> v .: "_version"
    <*> v .: "_source"

-------

  (.:) :: (FromJSON a) => Object -> Text -> Parser a
  .: => simplifies the lookup-and-parseJSON pattern.

  <$> => applies the result of a function to the parser result
         f <$> parserX
  <*> => if the function takes several parameters, you can keep applying with it (<*>).
-}


-- 11.16 Lists are polymorphic


-- 11.17 Binary Tree

data BinaryTree a
  = Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

insert' :: Ord a => a -> BinaryTree a -> BinaryTree a
insert' b Leaf = Node Leaf b Leaf
insert' b (Node left a right)
  | b == a = Node left a right
  | b < a = Node (insert' b left) a right
  | b > a = Node left a (insert' b right)


-- Write map for BinaryTree

mapTree :: (a -> b) -> BinaryTree a -> BinaryTree b
mapTree _ Leaf = Leaf
mapTree f (Node left a right) =
  -- Node undefined undefined undefined
  Node (mapTree f left) (f a) (mapTree f right)

testTree' :: BinaryTree Integer
testTree' = Node (Node Leaf 3 Leaf) 1 (Node Leaf 4 Leaf)

mapExpected = Node (Node Leaf 4 Leaf) 2 (Node Leaf 5 Leaf)

{-| acceptance test for mapTree -}
mapOkay =
  if mapTree (+1) testTree' == mapExpected
  then print "yup okay!"
  else error "test failed!"


-- Convert binary trees to lists
preorder :: BinaryTree a -> [a]
preorder Leaf = []
preorder (Node left a right) = [a] ++ inorder left ++ inorder right

inorder :: BinaryTree a -> [a]
inorder Leaf = []
inorder (Node left a right) = inorder left ++ [a] ++ inorder right

postorder :: BinaryTree a -> [a]
postorder Leaf = []
postorder (Node left a right) = inorder left ++ inorder right ++ [a]

testTree :: BinaryTree Integer
testTree = Node (Node Leaf 1 Leaf) 2 (Node Leaf 3 Leaf)

testPreorder :: IO ()
testPreorder =
  if preorder testTree == [2,1,3]
  then putStrLn "Preorder fine!"
  else putStrLn "Bad news bears"

testInorder :: IO ()
testInorder =
  if inorder testTree == [1,2,3]
  then putStrLn "Inorder fine!"
  else putStrLn "Bad news bears"

testPostorder :: IO ()
testPostorder =
  if postorder testTree == [1,3,2]
  then putStrLn "Postorder fine!"
  else putStrLn "postorder failed check"

main :: IO ()
main = do
  testPreorder
  testInorder
  testPostorder


-- Write foldr for BinaryTree
-- any traversal order is fine
foldTree :: (a -> b -> b) -> b -> BinaryTree a -> b
foldTree _ x Leaf = x
foldTree f x (Node left a right) = foldTree f (f a (foldTree f x left)) right

-- or using inorder
foldTree' f x t = foldr f x (inorder t)
