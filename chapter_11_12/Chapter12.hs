module Chapter12 where

-- 12.2 How to stop worrying and love Nothing

ifEvenAdd2 :: Integer -> Maybe Integer
ifEvenAdd2 n =
  if even n then Just (n + 2) else Nothing


-- Smart constructor for datatypes

type Name = String
type Age = Integer

data Person = Person Name Age deriving Show

mkPerson :: Name -> Age -> Maybe Person
mkPerson name age
  | name /= "" && age >= 0 = Just $ Person name age
  | otherwise = Nothing

{-|
  mkPerson is a smart constructor, meaning it allow us to construct values of a type only when they meet certain criteria.
-}


-- 12.3 Bleating either

data PersonInvalid
  = NameEmpty
  | AgeTooLow
  deriving (Eq, Show)

{-|
  Pattern matching is a case expression and pattern matching will work without an Eq instance, but guards using (==) will not.
-}

{-| Compiles without Eq -}
data PersonInvalid'
  = NameEmpty'
  | AgeTooLow'

toString :: PersonInvalid' -> String
toString NameEmpty' = "NameEmpty'"
toString AgeTooLow' = "AgeTooLow'"

instance Show PersonInvalid' where
  show = toString

{-| Does not work without an Eq instance -}
blah :: PersonInvalid' -> String
blah pi
  | pi == NameEmpty' = "NameEmpty'"
  | pi == AgeTooLow' = "AgeToLow"
  | otherwise = "???"

{-| now it should compile having an Eq instance -}
instance Eq PersonInvalid' where
  (==) NameEmpty' NameEmpty' = True
  (==) AgeTooLow' AgeTooLow' = True
  (==) NameEmpty' AgeTooLow' = False
  (==) AgeTooLow' NameEmpty' = False
  (/=) NameEmpty' AgeTooLow' = True
  (/=) AgeTooLow' NameEmpty' = True
  (/=) AgeTooLow' AgeTooLow' = False
  (/=) NameEmpty' NameEmpty' = False

{-| using Either -}
mkPerson' :: Name -> Age -> Either PersonInvalid Person
mkPerson' name age
  | name /= "" && age >= 0 = Right $ Person name age
  | name == "" = Left NameEmpty
  | otherwise = Left AgeTooLow

{-| showing list of errors-}
type ValidatePerson a = Either [PersonInvalid] a

ageOkay :: Age -> Either [PersonInvalid] Age
ageOkay age =
  case age >= 0 of
    True -> Right age
    False -> Left [AgeTooLow]

nameOkay :: Name -> Either [PersonInvalid] Name
nameOkay name =
  case name /= "" of
    True -> Right name
    False -> Left [NameEmpty]

mkPerson'' :: Name -> Age -> ValidatePerson Person
mkPerson'' name age =
  mkPerson''' (nameOkay name) (ageOkay age)

mkPerson''' :: ValidatePerson Name -> ValidatePerson Age -> ValidatePerson Person
mkPerson''' (Right validName) (Right validAge) =
  Right (Person validName validAge)
mkPerson''' (Left invalidName) (Left invalidAge) =
  Left (invalidName ++ invalidAge)
mkPerson''' (Left invalidName) _ = Left invalidName
mkPerson''' _ (Left invalidAge) = Left invalidAge

{-| using liftA2
mkPersonLifted :: Name -> Age -> Validation [PersonInvalid] Person
mkPersonLifted name age =
  liftA2 Peron (nameOkay name) (ageOkay age)
-}


-- 12.4 Kinds, a thousand stars in your types

{-|
  Type constant: type that take no arguments and are already types.
  Type constructor: refer to types which mush have arguments applied to become a type.

  :: => means "has type of". Used for type and kind signatures.
-}

data Trivial = Trivial
{-| :k Trivial => Trivial :: * -}

data Unary a = Unary a
{-| :k Unary => Unary :: * -> * -}

data TwoArgs a b = TwoArgs a b
{-| :k TwoArgs => TwoArgs :: * -> * -> * -}

data ThreeArgs a b c = ThreeArgs a b c
{-| :k ThreeArgs => ThreeArgs :: * -> * -> * -> * -}


-- Data constructors are functions

{-|
  VERY IMPORTANT:
  You can't hide polymorphic types from type constructors, so this is invalid:
  data Unary = Unary a deriving Show

  In order for the variable 'a' to be in scope, it needs to be introduced with the type constructor. So this is valid:
  data Unary a = Unary a deriving Show

  But when the type is not polymorphic then we can hide it from the type constructor. So this is valid:
  data Unary = Unary String deriving Show
-}
