module ChapterExercises where

import Data.Char as DC
import Data.List


-- Chapter 11

data Weekday
  = Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday

-- 1 => a

-- 2
f Friday = "Miller Time"
{-| c (f :: Weekday -> String) -}

-- 3 => b

-- 4
g xs = xs !! (length xs - 1)
{-| c (delivers the final element of xs) -}

-- Cipher: in a separated file

-- As-patterns

-- 1
isSubseqOf :: (Eq a) => [a] -> [a] -> Bool
isSubseqOf xs ys
  | isSubseqOf' xs ys == xs = True
  | otherwise = False
  where
    isSubseqOf' [] _ = []
    isSubseqOf' _ [] = []
    isSubseqOf' (x':xTail') ys'@(_:yTail') = filter (==x') ys' ++ isSubseqOf' xTail' yTail'
    {-| or
      isSubseqOf' xs' (y':yTail') = filter (==y') xs' ++ isSubseqOf' xs' yTail'
    -}

-- 2
{-|
  capitalizeWords :: String -> [(String, String)]
  capitalizeWords xs = [(map DC.toLower xs', map DC.toUpper xs') | xs' <- words xs]
-}

{-|
  capitalizeWords :: String -> [(String, String)]
  capitalizeWords xs = zip (words $ map DC.toLower xs) (words $ map DC.toUpper xs)
-}

capitalizeWords :: String -> [(String, String)]
capitalizeWords text = map (\word -> mkTuple word)(words text)
  where
    mkTuple :: String -> (String, String)
    mkTuple [] = ("","")
    mkTuple word@(letter:wordTail) = (word, DC.toUpper letter : wordTail)


-- Language exercices

-- 1
capitalizeWord :: String -> String
capitalizeWord [] = []
capitalizeWord (x:xs) = DC.toUpper x : xs

-- 2
capitalizeParagraph :: String -> String
capitalizeParagraph =
  foldl1 (\w1 w2 ->
    if last w1 == '.'
    then concat [w1, " ", capitalizeWord w2]
    else concat [w1, " ", w2]
  ) . words . capitalizeWord


-- Phone exercise

-- 1
data DaPhone = DaPhone [(Char, String)] deriving (Show, Eq)

phone = DaPhone
  [ ('1', "")
  , ('2', "abc")
  , ('3', "def")
  , ('4', "ghi")
  , ('5', "jkl")
  , ('6', "mno")
  , ('7', "pqrs")
  , ('8', "tuv")
  , ('9', "wxyz")
  , ('0', "+_")
  , ('*', "^")
  , ('#', ".,")
  ]

-- 2
convo :: [String]
convo =
  ["Wanna play 20 questions"
  , "Ya"
  , "U 1st haha"
  , "Lol ok. Have u ever tasted alcohol"
  , "Lol ya"
  , "Wow ur cool haha. Ur turn"
  , "Ok. Do u thing I am pretty Lol"
  , "Lol, ya"
  , "Just making sure rofl ur turn"
  ]

-- validButtons = "1234567890*#"
type Digit = Char

-- valid presses: 1 and up
type Presses = Int

reverseTaps :: DaPhone -> Char -> [(Digit, Presses)]
reverseTaps (DaPhone p) c
  | isUpper c = ('*', 1) : getButton p (toLower c)
  | isSpace c = [('0', 1)]
  | otherwise = getButton p c
  where
    getButton p' c'=
      [(f', getPresses c' s' 1)
      | (f',s') <- p'
      , c' `elem` s'
      ]

getPresses :: Char -> String -> Int -> Int
getPresses char (x:xs) counter
  | x == char = counter
  | otherwise = getPresses char xs (counter + 1)

cellPhonesDead :: DaPhone -> String -> [(Digit, Presses)]
cellPhonesDead p = concatMap (reverseTaps p)

-- 3
fingerTaps :: [(Digit, Presses)] -> Presses
fingerTaps xs = foldr ((+) . snd) 0 xs

-- 4
mostPopularLetter :: String -> Char
mostPopularLetter xs =
  head
  $ foldl1 (\a b -> if length a >= length b then a else b)
  $ go
  $ filter (not . isSpace) xs
  where
    go [] = []
    go (y:ys) = [filter (==y) ys] ++ go (filter (/=y) ys) ++ []


-- Hutton's Razor

-- 1
data Expr
  = Lit Integer
  | Add Expr Expr

eval :: Expr -> Integer
eval (Lit x) = x
eval (Add expr expr') = eval expr + eval expr'

-- 2
printExpr :: Expr -> String
printExpr (Lit x) = show x
printExpr (Add expr expr') = printExpr expr ++ " + " ++ printExpr expr'


-- Chapter 12

-- 1
{-|
  id :: a -> a
  a :: *
-}

-- 2
{-|
  r :: a -> f a
  a :: *
  f :: * -> *
-}


-- String processing

-- 1
notThe :: String -> Maybe String
notThe s
  | s /= "the" = Just s
  | otherwise = Nothing

replaceThe :: String -> String
replaceThe =
  unwords .
  mapReplace .
  words
  where
    mapReplace [] = []
    mapReplace (x:xs) =
      case notThe x of
        Just _ -> x
        _ -> "a"
      : mapReplace xs
      {-|
      map (\w -> case notThe w of
        Just _ -> w
        _ -> "a"
      )
      -}

-- 2
countTheBeforeVowel :: String -> Integer
countTheBeforeVowel =
  go 0 . words
  where
    go _ [] = 0
    go counter xs = counter + go (newCounter xs) (tail xs)
    newCounter (x:(y:_))
      | isThe x && nextWordStartsWithVowel y = 1
      | otherwise = 0
    newCounter _ = 0

isThe :: String -> Bool
isThe w
  | w == "the" = True
  | otherwise = False

nextWordStartsWithVowel :: String -> Bool
nextWordStartsWithVowel (l:_) = l `elem` vowels
nextWordStartsWithVowel _ = False

vowels :: String
vowels = "aeiou"

-- 3
countVowels :: String -> Integer
countVowels =
  toInteger . length . filter isVowel

isVowel :: Char -> Bool
isVowel l = l `elem` vowels


-- Validate the word
newtype Word' = Word' String deriving (Eq, Show)

mkWord :: String -> Maybe Word'
mkWord t =
  if vowelsLength t > consonantLength t
  then Nothing
  else Just (Word' (count t))
  where
    vowelsLength = calculateLength (filter isVowel)
    consonantLength = calculateLength (filter (not . isVowel))
    count t' = "Vowels: " ++ (show $ vowelsLength t') ++ " | Consonants: " ++ show (consonantLength t')
    calculateLength predicate = toInteger . length . predicate . filter isAlpha


-- It's only Natural
-- natural to integers won't return maybe because natural is contained in integers
data Nat
  = Zero
  | Succ Nat
  deriving (Eq, Show)

natToInteger :: Nat -> Integer
natToInteger Zero = 0
natToInteger (Succ nat) = natToInteger nat + 1

integerToNat :: Integer -> Maybe Nat
integerToNat x
  | x >= 0 = Just (go x)
  | otherwise = Nothing
  where
    go x'
      | x' > 0 = Succ (go (x' - 1))
      | otherwise = Zero


-- Small library for Maybe

-- 1
isJust :: Maybe a -> Bool
isJust (Just _) = True
isJust Nothing = False

isNothing :: Maybe a -> Bool
isNothing = not . isJust

-- 2
mayybe :: b -> (a -> b) -> Maybe a -> b
mayybe x h m =
  case m of
    Just y -> h y
    Nothing -> x

-- 3
fromMaybe :: a -> Maybe a -> a
fromMaybe _ (Just x) = x
fromMaybe y Nothing = y

-- 4
listToMaybe :: [a] -> Maybe a
listToMaybe [] = Nothing
listToMaybe (x:_) = Just x

maybeToList :: Maybe a -> [a]
maybeToList m =
  case m of
    Just x -> [x]
    Nothing -> []

-- 5
catMaybes :: [Maybe a] -> [a]
catMaybes [] = []
catMaybes (x:xs) =
  case x of
    Just x' -> x' : catMaybes xs
    Nothing -> catMaybes xs

-- 6
flipMaybe :: [Maybe a] -> Maybe [a]
flipMaybe [] = Nothing
flipMaybe xs =
  if any isNothing xs
  then
    Nothing
  else
    Just (go xs)
  where
    go [] = []
    go (y:ys) =
      case y of
        Just y' -> y' : go ys
        Nothing -> go ys


-- Small library for Either

-- 1
lefts' :: [Either a b ] -> [a]
lefts' =
  foldr (\x y -> case x of
    Left x' -> x' : y
    Right _ -> y
  ) []

-- 2
rights' :: [Either a b] -> [b]
rights' =
  foldr (\x y -> case x of
    Right x' -> x' : y
    Left _ -> y
  ) []

-- 3
partitionEithers' :: [Either a b ] -> ([a], [b])
partitionEithers' xs = (lefts' xs, rights' xs)

-- 4
eitherMaybe' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe' _ (Left _) = Nothing
eitherMaybe' f4 (Right r) = Just (f4 r)

-- 5
either' :: (a -> c) -> (b -> c) -> Either a b -> c
either' f5 g5 e =
  case e of
    Left x -> f5 x
    Right x -> g5 x

-- 6
eitherMaybe'' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe'' f6 =
  either' (\_ -> Nothing) (\x -> Just (f6 x))


-- Unfolds

mehSum :: Num a => [a] -> a
mehSum xs = go 0 xs
  where
    go :: Num a => a -> [a] -> a
    go n [] = n
    go n (x:xs) = go (n+x) xs

niceSum :: Num a => [a] -> a
niceSum = foldl' (+) 0

mehProduct :: Num a => [a] -> a
mehProduct xs = go 1 xs
  where
    go :: Num a => a -> [a] -> a
    go n [] = n
    go n (x:xs) = go (n*x) xs

niceProduct :: Num a => [a] -> a
niceProduct = foldl' (*) 1

mehConcat :: [[a]] -> [a]
mehConcat xs = go [] xs
  where
    go :: [a] -> [[a]] -> [a]
    go xs' [] = xs'
    go xs' (x:xs) = go (xs' ++ x) xs

niceConcat :: [[a]] -> [a]
niceConcat = foldr (++) []


-- Write your own iterate and unfoldr

-- 1
myIterate :: (a -> a) -> a -> [a]
myIterate fI x = [x] ++ myIterate fI (fI x)

-- 2
myUnfoldr :: (b -> Maybe (a,b)) -> b -> [a]
myUnfoldr fU b =
  case fU b of
    Just (a, b') -> [a] ++ myUnfoldr fU b'
    Nothing -> []

-- 3
betterIterate :: (a -> a) -> a -> [a]
betterIterate fBI = myUnfoldr (\b -> Just (b, fBI b))


-- Finally something other than a list!
data BinaryTree' a
  = Leaf'
  | Node' (BinaryTree' a) a (BinaryTree' a)
  deriving (Eq, Ord, Show)

-- 1
unfold :: (a -> Maybe(a,b,a)) -> a -> BinaryTree' b
unfold fU' x =
  case fU' x of
    Just (a, b, c) -> Node' (unfold fU' a) b (unfold fU' c)
    Nothing -> Leaf'

-- 2
treeBuild :: Integer -> BinaryTree' Integer
treeBuild =
  unfold (\x -> if x == 0 then Nothing else Just(x-1, x-1, x-1))
