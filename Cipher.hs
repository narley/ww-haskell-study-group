module Cipher where

import Data.Char

lowerCharToInt :: Char -> Int
lowerCharToInt c = ord c - ord 'a'

intToLowerChar :: Int -> Char
intToLowerChar n = chr (ord 'a' + n)

upperCharToInt :: Char -> Int
upperCharToInt c = ord c - ord 'A'

intToUpperChar :: Int -> Char
intToUpperChar n = chr (ord 'A' + n)

changeChar :: (Int -> Int -> Int) -> Int -> Char -> Char
changeChar f n c
  | isLower c = intToLowerChar ((lowerCharToInt c `f` n) `mod` 26)
  | isUpper c = intToUpperChar ((upperCharToInt c `f` n) `mod` 26)
  | otherwise = c

caesar :: Int -> String -> String
caesar n xs = [changeChar (+) n x | x <- xs]

unCaesar :: Int -> String -> String
unCaesar n xs = [changeChar (-) n x | x <- xs]

-- TODO: to be completed...
vigenere :: String -> String -> String
vigenere keyword text = undefined
