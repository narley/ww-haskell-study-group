module Chapter8 where


-- 8.2

factorial :: integer -> integer
factorial 0 = 1
factorial n = n * factorial (n - 1)


inctimes :: (eq a, num a) => a -> a -> a
inctimes 0 n = n
inctimes times n =
  -- 1 + (inctimes (times - 1) n)
  inctimes (times - 1) ( n + 1 )


applytimes :: (eq a, num a) => a -> (b -> b) -> b -> b
applytimes 0 f b = b
applytimes n f b = f (applytimes (n-1) f b)

inctimes' :: (eq a, num a) => a -> a -> a
inctimes' times n = applytimes times (+1) n

applytimes' :: (eq a, num a) => a -> (b -> b) -> b -> b
applytimes' 0 f b = b
applytimes' n f b =
  f . applytimes (n-1) f $ b

inctimes'' :: (eq a, num a) => a -> a -> a
inctimes'' times n = applytimes' times (+1) n


-- exercise

-- applytimes 5 (+1) 5
-- (+1) (applytimes (5 -1) (+1) 5) => (applytimes 4 (+1) 5) + 1
-- (+1) (applytimes (4-1) (+1) 5) + 1 => ((applytimes 3 (+1) 5) + 1) + 1
-- (+1) ((applytimes (3-1) (+1) 5) + 1) + 1 => (((applytimes 2 (+1) 5) + 1) + 1) + 1
-- (+1) (((applytimes (2-1) (+1) 5) + 1) + 1) + 1 => ((((applytimes 1 (+1) 5) + 1) + 1) + 1) + 1
-- (+1) ((((applytimes (1-1) (+1) 5) + 1) + 1) + 1) + 1 => (((((applytimes 0 (+1) 5) + 1) + 1) + 1) + 1) + 1
-- ((((5 + 1) + 1) + 1) + 1) + 1
-- (((6 + 1) + 1) + 1) + 1
-- ((7 + 1) + 1) + 1
-- (8 + 1) + 1
-- 9 + 1
-- 10


-- 8.4

fibonacci :: integral a => a -> a
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci x = fibonacci (x - 1) + fibonacci (x - 2)


-- 8.5

type numerator = integer
type denominator = integer
type quocient = integer

divideby :: numerator -> denominator -> quocient
divideby = div

divideby' :: Integral a => a -> a -> (a,a)
divideby' num denom = go num denom 0
  where
    go n d count
      | n < d = (count, n)
      | otherwise = go (n - d) d (count + 1)
