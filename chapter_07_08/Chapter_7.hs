module Chapter7 where


-- Exercises: Grab Bag

-- 1
mTha x y z = x*y*z
mThb x y = \z -> x*y*z
mThc x = \y -> \z -> x*y*z
mThd = \x -> \y -> \z -> x*y*z
-- all equivalent

-- 2
-- d

-- 3
-- a)
addOneIfOdd n = case odd n of
  True -> f n
  False -> n
  where f n = n + 1

addOneIfOdd' n = case odd n of
  True -> f n
  False -> n
  where f = \n -> n + 1

-- b
addFive x y = (if x > y then y else x) + 5
addFive' = \x -> \y -> (if x > y then y else x) + 5

-- c
mflip f = \x -> \y -> f y x
mflip' f x y = f y x


-- 7.4

newtype Username =
  Username String

newtype AccountNumber =
  AccountNumber Integer

data User
  = UnregisteredUser
  | RegisteredUser Username AccountNumber

printUser :: User -> IO ()
printUser UnregisteredUser =
  putStrLn "UnregisteredUser"
printUser (RegisteredUser
          (Username name)
          (AccountNumber acctNum)) =
  putStrLn $ name ++ " " ++ show acctNum


-- Pattern Matching: data constructors
data WherePenguinsLive
  = Galapagos
  | Antarctica
  | Australia
  | SouthAfrica
  | SouthAmerica
  deriving (Eq, Show)

data Penguin =
  Peng WherePenguinsLive
  deriving (Eq, Show)

-- is it South Africa? If so, return True
isSouthAfrica :: WherePenguinsLive -> Bool
isSouthAfrica SouthAfrica = True
isSouthAfrica _ = False

gimmeWhereTheyLive :: Penguin -> WherePenguinsLive
gimmeWhereTheyLive (Peng whereItLives) = whereItLives

humboldt = Peng SouthAmerica
gentoo = Peng Antarctica
macaroni = Peng Antarctica
little = Peng Australia
galapagos = Peng Galapagos

galapagosPenguin :: Penguin -> Bool
galapagosPenguin (Peng Galapagos) = True
galapagosPenguin _ = False

antarcticaPenguin :: Penguin -> Bool
antarcticaPenguin (Peng Antarctica) = True
antarcticaPenguin _ = False

antarcticOrGalapagos :: Penguin -> Bool
antarcticOrGalapagos p = (galapagosPenguin p) || (antarcticaPenguin p)


-- Pattern Matching: tuples
-- These have to be the same type because (+) is a -> a-> a
addEmUp2 :: Num a => (a, a) -> a
addEmUp2 (x, y) = x + y

-- addEmUp2 could also be written like so
addEmUp2Alt :: Num a => (a, a) -> a
addEmUp2Alt tup = (fst tup) + (snd tup)

fst3 :: (a, b, c) -> a
fst3 (x,_,_) = x

third3 :: (a, b, c) -> c
third3 (_,_,x) = x


-- Exercises: Variety Pack

-- 1
k (x,y) = x
k1 = k ((4-1), 10)
k2 = k ("three", (1+2))
k3 = k (3, True)

-- a
k :: (a, b) -> a

-- b
k2 :: [Char]
k1 :: Integer
k3 :: Integer
-- k2 is not the same type as k1 or k3

-- c
-- k1 and k3

-- 2
f :: (a,b,c) -> (d,e,f) -> ((a,d), (c,f))
f (a,_,c) (d,_,f) = ((a,d), (c,f))


-- 7.5

funcZ :: (Num a, Eq a) => a -> [Char]
funcZ x =
  case x + 1 == 1 of
    True -> "AWESOME"
    False -> "wut"

pal :: [Char] -> [Char]
pal xs =
  case xs == reverse xs of
    True -> "yes"
    False -> "no"

pal' :: [Char] -> [Char]
pal' xs =
  case y of
    True -> "yes"
    False -> "no"
  where y = xs == reverse xs

greetIfCool :: String -> IO ()
greetIfCool coolness =
  case cool of
    True ->
      putStrLn "eyyyyy. What's shakin'?"
    False ->
      putStrLn "pshhh."
  where cool = coolness == "downright frosty yo"


-- Exercises: Case Practice

-- 1
functionC :: (Ord a, Ord a) => a -> a -> a
functionC x y = if (x > y) then x else y
functionC' x y =
  case x > y of
    True -> x
    False -> y

-- 2
ifEvenAdd2 :: Integral a => a -> a
ifEvenAdd2 n = if even n then n+2 else n
ifEvenAdd2' n =
  case even n of
    True -> n+2
    False -> n

-- 3
nums :: (Num a, Ord a) => a -> a
nums x =
  case compare x 0 of
    LT -> -1
    GT -> 1
    EQ -> 0


-- 7.6

data Employee
  = Coder
  | Manager
  | Veep
  | CEO
  deriving (Eq, Ord, Show)

reportBoss :: Employee -> Employee -> IO ()
reportBoss e e' =
  putStrLn $ show e ++ " is the boss of " ++ show e'

employeeRank :: Employee -> Employee -> IO ()
employeeRank e e' =
  case compare e e' of
    GT -> reportBoss e e'
    EQ -> putStrLn "Neither employee\
                   \ is the boss"
    LT -> (flip reportBoss) e e'

employeeRank' :: (Employee -> Employee -> Ordering) -> Employee -> Employee -> IO ()
employeeRank' f e e' =
  case f e e' of
    GT -> reportBoss e e'
    EQ -> putStrLn "Neither employee\
                   \ is the boss"
    LT -> reportBoss e' e

codersRuleCEOsDrool :: Employee -> Employee -> Ordering
codersRuleCEOsDrool Coder Coder = EQ
codersRuleCEOsDrool Coder _ = GT
codersRuleCEOsDrool _ Coder = LT
codersRuleCEOsDrool e e' = compare e e'


-- Exercises: Artful Dodgy

dodgy x y = x + y * 10
oneIsOne = dodgy 1
oneIsTwo = (flip dodgy) 2

-- 1
one = dodgy 1 0
-- 1

-- 2
two = dodgy 1 1
-- 11

-- 3
three = dodgy 2 2
-- 22

-- 4
four = dodgy 1 2
-- 21

-- 5
five = dodgy 2 1
-- 12

-- 6
six = oneIsOne 1
-- 11

-- 7
seven = oneIsOne 2
-- 21

-- 8
eight = oneIsTwo 1
-- 21

-- 9
nine = oneIsTwo 2
-- 22

-- 10
ten = oneIsOne 3
-- 31

-- 11
eleven = oneIsTwo 3
-- 23


-- 7.7

myAbs :: Integer -> Integer
myAbs x
  | x < 0 = (-x)
  | otherwise = x

bloodNa :: Integer -> String
bloodNa x
  | x < 135 = "too low"
  | x > 145 = "too high"
  | otherwise = "just right"


-- c is the hypotenuse of the triangle
isRight :: (Num a, Eq a) => a -> a -> a -> String
isRight a b c
  | a^2 + b^2 == c^2 = "RIGHT ON"
  | otherwise = "not right"

dogYrs :: Integer -> Integer
dogYrs x
  | x <= 0 = 0
  | x <= 1 = x * 15
  | x <= 2 = x * 12
  | x <= 4 = x * 8
  | otherwise = x * 6

avgGrade :: (Fractional a, Ord a) => a -> Char
avgGrade x
  | y >= 0.9 = 'A'
  | y >= 0.8 = 'B'
  | y >= 0.7 = 'C'
  | y >= 0.59 = 'D'
  | otherwise = 'F'
  where y = x / 100


-- Exercises: Guard Duty

-- 1
avgGrade' :: (Fractional a, Ord a) => a -> Char
avgGrade' x
  | otherwise = 'F'
  | y >= 0.9 = 'A'
  | y >= 0.8 = 'B'
  | y >= 0.7 = 'C'
  | y >= 0.59 = 'D'
  where y = x / 100
-- it will always return 'F' if otherwise if the first item in the guard.

-- 2
avgGrade'' :: (Fractional a, Ord a) => a -> Char
avgGrade'' x
  | y >= 0.7 = 'C'
  | y >= 0.9 = 'A'
  | y >= 0.8 = 'B'
  | y >= 0.59 = 'D'
  | otherwise = 'F'
  where y = x / 100

-- 3
pal3 xs
  | xs == reverse xs = True
  | otherwise = False
-- b

-- 4
-- can take a list of things of same type

-- 5
pal3 :: Eq a => [a] -> Bool

-- 6
numbers x
  | x < 0 = -1
  | x == 0 = 0
  | x > 0 = 1
-- returns c

-- 7
-- 'numbers' cant take arguments that has an instance of Eq and Ord

-- 8
-- numbers :: (Ord a, Num a, Num p) => a -> p


-- 7.9

foo1 :: [Char] -> Int
foo1 = length . filter (== 'a')

add :: Int -> Int -> Int
add x y = x + y

addPF :: Int -> Int -> Int
addPF = (+)

addOne :: Int -> Int
addOne = \x -> x + 1

addOnePF :: Int -> Int
addOnePF = (+1)

main1 :: IO ()
main1 = do
  print (0 :: Int) -- 0
  print (add 1 0) -- 1
  print (addOne 0) -- 1
  print (addOnePF 0) -- 1
  print ((addOne . addOne) 0) -- 2
  print ((addOnePF . addOne) 0) -- 2
  print ((addOne . addOnePF) 0) -- 2
  print (negate (addOne 0)) -- -1
  print ((negate . addOne) 0) -- -1
  print ((addOne . addOne . addOne . negate . addOne) 0) -- 2
