module ChapterExercises where

import Data.List (intercalate)

-- Chapter 7

-- Multiple choice

-- 1 = d
-- 2 = b
-- 3 = d
-- 4 = b
-- 5 = a


-- Let's write code

-- 1
tensDigit :: Integral a => a -> a
tensDigit x = d
  where xLast = x `div` 10
        d = xLast `mod` 10

-- a)
tensDigit1 :: Integral a => a -> a
tensDigit1 = snd . divMod' . fst . divMod'
  where divMod' = flip divMod 10

-- b)
-- Yes. It has the same type.

-- c)
hunsD :: Integral a => a -> a
hunsD =
  snd . divMod' . fst . divMod' . fst . divMod'
  where divMod' = flip divMod 10

-- 2
foldBool :: a -> a -> Bool -> a
foldBool = error "Error: Need to implement foldBool!"

-- using pattern matching
foldBool3 :: a -> a -> Bool -> a
foldBool3 x _ False = x
foldBool3 _ y True = y

-- using case expression
foldBool1 :: a -> a -> Bool -> a
foldBool1 x y bool =
  case bool of
    True -> y
    False -> x

-- using guards
foldBool2 :: a -> a -> Bool -> a
foldBool2 x y bool
  | bool = y
  | otherwise = x

-- 3
g :: (a -> b) -> (a,c) -> (b,c)
g f (a,c) = (f a, c)

-- 4
-- id :: a -> a
-- id  = x

roundTrip :: (Show a, Read a) => a -> a
roundTrip a = read (show a)

main4 :: IO ()
main4 = do
  print (roundTrip 4)
  print (id 4)

-- 5
roundTrip5 :: (Show a, Read a) => a -> a
roundTrip5 = read . show

main5 :: IO ()
main5 = do
  print (roundTrip5 5)
  print (id 5)

-- 6
roundTrip6 :: (Show a, Read b) => a -> b
roundTrip6 = read . show

main6 :: IO ()
main6 = do
  print (roundTrip6 6 :: Int)
  print (id 6)


-- Chapter 8

-- Review of types

-- 1
-- [[True, False], [True, True], [False, True]]
-- d => [[Bool]]

-- 2
-- [[True, False], [True, True], [False, True]]
-- b => [[3 == 3], [6 > 5], [3 < 4]]

-- 3
-- d

-- 4
-- func :: [a] -> [a] -> [a]
-- func x y = x ++ y
-- b => func "Hello" "World"


-- Reviewing currying

cattyConny :: String -> String -> String
cattyConny x y = x ++ " mrow " ++ y

flippy :: String -> String -> String
flippy = flip cattyConny

appedCatty :: String -> String
appedCatty = cattyConny "woops"

frappe :: String -> String
frappe = flippy "haha"

-- 1
fone = appedCatty "woohoo!" -- "Woops mrow woohoo!"

-- 2
ftwo = frappe "1" -- "1 mrow haha"

-- 3
fthree = frappe (appedCatty "2") -- "woops mrow 2 mrow haha"

-- 4
ffour = appedCatty (frappe "blue") -- "woops mrow blue mrow haha"

-- 5
ffive = cattyConny (frappe "pink") (cattyConny "green" (appedCatty "blue")) -- "pink mrow haha mrow green mrow woops mrow blue"
-- "pink mrow haha"
-- "green" "woops mrow blue" => "green mrow woops mrow blue"

-- 6
fsix = cattyConny (flippy "Pugs" "are") "awesome" -- "are mrow Pugs mrow awesome"
-- "are mrow Pugs"


-- Recursion

-- 1
-- divideBy 15 2 =
-- go 15 2 0
-- | 15 < 2 = False -- skip
-- | otherwise = go (15-2) 2 (0+1)

-- go 13 2 1
-- | 13 < 2 = False -- skip
-- | otherwise = go (13-2) 2 (1+1)

-- go 11 2 2
-- | 11 < 2 = False -- skip
-- | otherwise = go (11-2) 2 (2+1)

-- go 9 2 3
-- | 9 < 2 = False -- skip
-- | otherwise = go (9-2) 2 (3+1)

-- go 7 2 4
-- | 7 < 2 = False -- skip
-- | otherwise = go (7-2) 2 (4+1)

-- go 5 2 5
-- | 5 < 2 = False -- skip
-- | otherwise = go (5-2) 2 (5+1)

-- go 3 2 6
-- | 3 < 2 = False -- skip
-- | otherwise = go (3-2) 2 (6+1)

-- go 1 2 7
-- | 1 < 2 = (7,1)

-- 2
recFuncTwo :: (Eq a, Num a) => a -> a
recFuncTwo x = go x 0
  where
    go n t
      | n == 0 = t
      | otherwise = go (n-1) (t+n)

-- 3
recFuncThree :: Integral a => a -> a -> a
recFuncThree x y = go x y 0
  where
    go n m t
      | m == 0 = t
      | otherwise = go n (m-1) (t+n)


-- Fixing dividedBy

data DividedResult
  = Result Integer
  | DividedByZero
  deriving (Show)

dividedBy :: Integer -> Integer -> DividedResult
dividedBy num denom = go num num denom 0
  where
    go n' n d count
      | d == 0 = DividedByZero
      | n' < 0 || d < 0 =
          if abs n < abs d
          then Result (-count)
          else go n (-(abs n - abs d)) d (count + 1)
      | otherwise =
          if n < d
          then Result count
          else go n (n - d) d (count + 1)

-- Group solution
dividedBy' :: Integer -> Integer -> DividedResult
dividedBy' _ 0 = DividedByZero
dividedBy' n d = Result $ (dividedBy'' (abs n) (abs d)) * (sign n d)
  where
    sign n d = if (n * d < 0) then -1 else 1
    dividedBy'' num denom = go num denom 0
      where
        go n' d' count
          | n' < d' = count
          | otherwise =
              go (n' - d') d' (count + 1)


-- McCarthy 91 function

mc91 :: Integer -> Integer
mc91 n
  | n > 100 = n - 10
  | otherwise = mc91 $ mc91 $ n + 11


-- Numbers into words

digitToWord :: Int -> String
digitToWord n
  | n == 1 = "one"
  | n == 2 = "two"
  | n == 3 = "three"
  | n == 4 = "four"
  | n == 5 = "five"
  | n == 6 = "six"
  | n == 7 = "seven"
  | n == 8 = "eight"
  | n == 9 = "nine"
  | otherwise = "zero"

digits :: Int -> [Int]
digits n = go n []
  where
    go x xs
      | div x 10 == 0 = x : xs
      | otherwise = go (div x 10) (mod x 10 : xs)

wordNumber :: Int -> String
wordNumber n =
  intercalate "-" $ map digitToWord $ digits n
