module Chapter9 where

import Data.Maybe
import Data.Bool
import Data.Char


-- 9.3

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail (_:[]) = Nothing
safeTail (_:xs) = Just xs


safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x


-- Exercise: EnumFromTo

-- 1
eftBool :: Bool -> Bool -> [Bool]
eftBool x y
  | x > y = []
  | x == y = [x]
  | otherwise = [x,y]

-- 2
etfOrd :: Ordering -> Ordering -> [Ordering]
etfOrd x y
  | x > y = []
  | x == y = [x]
  | otherwise = x : etfOrd (succ x) y

-- 3
eftInt :: Int -> Int -> [Int]
eftInt x y
  | x > y = []
  | x == y = [x]
  | otherwise = x : eftInt (succ x) y

-- 4
eftChar :: Char -> Char -> [Char]
eftChar x y
  | x > y = []
  | x == y = [x]
  | otherwise = x : eftChar (succ x) y


-- 9.6

-- Exercises: Thy Fearful Symmetry

-- 1
myWords :: String -> [String]
myWords txt = go txt []
  where
    go t xs'
      | t == [] = xs'
      | otherwise = takeWord t : go (newText t) []
    newText = drop 1 . dropWhile (/=' ')
    takeWord = takeWhile (/=' ')

-- 2
firstSen = "Tyger Tyger, burning bright\n"
secondSen = "In the forests of the night\n"
thirdSen = "What immortal hand or eye\n"
fourthSen = "Could frame thy fearful symmetry?"
sentences = firstSen ++ secondSen ++ thirdSen ++ fourthSen

shouldEqual =
  [ "Tyger Tyger, burning bright"
  , "In the forests of the night"
  , "What immortal hand or eye"
  , "Could frame thy fearful symmetry?"
  ]

myLines :: String -> [String]
myLines txt = go txt []
  where
    go t xs'
      | t == [] = xs'
      | otherwise = takeWord t : go (newText t) []
    newText = drop 1 . dropWhile (/='\n')
    takeWord = takeWhile (/='\n')

main2 :: IO ()
main2 =
  print $
  "Are they equal? " ++ show (myLines sentences == shouldEqual)

-- 3
breakAt :: Char -> String -> [String]
breakAt _ [] = []
breakAt c t = takeWhile (/=c) t : breakAt c (drop 1 $ dropWhile (/=c) t)


myWords' :: String -> [String]
myWords' = breakAt ' '

myLines' :: String -> [String]
myLines' = breakAt '\n'

main2' :: IO ()
main2' =
  print $
  "Are they equal? " ++ show (myLines' sentences == shouldEqual)


-- 9.7
mySqr = [x^2 | x <- [1..10]]
-- [1,4,9,16,25,36,49,64,81,100]


-- Exercises: Comprehend Thy Lists

-- 1
one = [x | x <- mySqr, rem x 2 == 0]
-- [4,16,36,64,100]

-- 2
two = [(x,y) | x <- mySqr, y <- mySqr, x < 50, y > 50]
--[(1,64),(1,81),(1,100)
--,(4,64),(4,81),(4,100)
--,(9,64),(9,81),(9,100)
--,(16,64),(16,81),(16,100)
--,(25,64),(25,81),(25,100)
--,(36,64),(36,81),(36,100)
--,(49,64),(49,81),(49,100)
--]

-- 3
three = take 5 [(x,y) | x <- mySqr
               , y <- mySqr
               , x < 50, y > 50
               ]
--[(1,64),(1,81),(1,100)
--,(4,64),(4,81)
--]


-- List comprehensions with Strings

acro :: String -> String
acro xs = [x | x <- xs, elem x ['A'..'Z']]

onlyVowels :: String -> String
onlyVowels xs = [x | x <- xs, elem x "aeiou"]


-- Exercises: Square Cube

mySqrTwo = [x^2 | x <- [1..5]] -- [1,4,9,16,25]
myCube = [y^3 | y <- [1..5]] -- [1,8,27,64,125]

-- 1
tuple1 = [(x,y) | x <- mySqrTwo, y <- myCube]

-- 2
tuple2 = [(x,y) | x <- mySqrTwo, y <- myCube, x < 50, y < 50]

-- 3
tuple3 = length tuple2


-- 9.8
-- Not identical to the length function in prelude
length' :: [a] -> Integer
length' [] = 0
length' (_:xs) = 1 + length' xs

sum' :: Num a => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs


-- Exercises: Bottom Madness

-- Will it blow up?
--1
wbu1 = [x^y | x<-[1..5], y<-[2,undefined]] -- bottom

-- 2
wbu2 = take 1 $ [x^y | x<-[1..5], y<-[2,undefined]] -- [1]

-- 3
wbu3 = sum [1, undefined, 3] -- bottom

-- 4
wbu4 = length [1,2,undefined] -- 3

-- 5
wbu5 = length $ [1,2,3] ++ undefined -- bottom

-- 6
wbu6 = take 1 $ filter even [1,2,3,undefined] -- bottom / [2]

-- 7
wbu7 = take 1 $ filter even [1,3,undefined] -- bottom

-- 8
wbu8 = take 1 $ filter odd [1,3,undefined] -- bottom / [1]

-- 9
wbu9 = take 2 $ filter odd [1,3,undefined] -- bottom / [1,3]

-- 10
wbu10 = take 3 $ filter odd [1,3,undefined] -- bottom


-- Is it in normal form?
-- 1
-- [1,2,3,4,5] -- NF & WHNF

-- 2
-- 1:2:3:4:_ -- WHNF

-- 3
-- enumFromTo 1 10 -- Neither

-- 4
-- length [1,2,3,4,5] -- Neither

-- 5
-- sum (enumFrom 1 10) -- Neither

-- 6
-- ['a'..'m'] ++ ['n'..'z'] -- Neither

-- 7
-- (_,'b') -- NF & WHNF


-- 9.9

-- Exercises: More Bottoms

-- 1
mb1 = take 1 $ map (+1) [undefined,2,3] -- bottom

-- 2
mb2 = take 1 $ map (+1) [1, undefined, 3] -- [2]

-- 3
mb3 = take 2 $ map (+1) [1, undefined, 3] -- bottom

-- 4
itIsMystery :: String -> [Bool]
itIsMystery xs = map (\x -> elem x "aeiou") xs
-- this function takes a String and returns a list of Booleans. For each letter in the string it will return True if is a vowel, False otherwise.

-- 5
-- a
mb5a = map (^2) [1..10] -- [1,4,9,16,25,36,49,64,81,100]

-- b
mb5b = map minimum [[1..10],[10..20],[20..30]] -- [1,10,20]

-- c
mb5c = map sum [[1..5],[1..5],[1..5]] -- [15,15,15]

-- 6
mb6 :: (Eq a, Num a) => [a] -> [a]
mb6 = map (\x -> bool x (-x) (x == 3))


-- 9.10

-- Exercises: Filtering

-- 1
filterOne :: [Integer] -> [Integer]
filterOne = filter (\x -> rem x 3 == 0)

-- 2
filterTwo :: [Integer] -> Int
filterTwo = length . filterOne

-- 3
filterThree :: String -> [String]
filterThree = filter (\x -> lowerString x `notElem` ["the", "a", "an"]) . words
  where
    lowerString xs = [toLower x | x <- xs]


-- 9.11

-- Exercises: Zipping

-- 1
myZip :: [a] -> [b] -> [(a,b)]
myZip _ [] = []
myZip [] _ = []
myZip (x:xs) (y:ys) = (x,y) : myZip xs ys

-- 2
myZipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
myZipWith _ _ [] = []
myZipWith _ [] _ = []
myZipWith f (x:xs) (y:ys) = f x y : myZipWith f xs ys

-- 3
myZip' :: [a] -> [b] -> [(a,b)]
myZip' _ [] = []
myZip' [] _ = []
myZip' xs ys = myZipWith (,) xs ys
