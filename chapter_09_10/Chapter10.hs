module Chapter10 where


import Data.Time


myAny :: (a -> Bool) -> [a] -> Bool
myAny f xs =
  foldr (\x y -> f x || y) False xs


-- 10.5
-- Associativity and folding

-- foldr (^) 2 [1..3]
-- (1 ^ (2 ^ (3 ^ 2)))
-- (1 ^ (2 ^ 9))
-- 1 ^ 512
-- 1

-- foldl (^) 2 [1..3]
-- (((2 ^ 1) ^ 2) ^ 3)
-- ((2 ^ 2) ^ 3)
-- 4 ^ 3
-- 64


-- Exercises: Understanding Folds

-- 1
-- foldr (*) 1 [1..5] same as:
-- b) foldl (flip (*)) 1 [1..5]
-- c) foldl (*) 1 [1..5]

-- 2
-- foldl (flip (*)) 1 [1..3]
-- (((1 * 1) * 2) * 3)

-- 3
-- c) foldr, but not foldl, associates to the right

-- 4
-- a) reduce structure

-- 5
-- a) foldr (++) ["woot", "WOOT", "woot"]
fiveA = foldr (++) [] ["woot", "WOOT", "woot"]

-- b) foldr max [] "fear is the little death"
fiveB = foldr max ' ' "fear is the little death"

-- c) foldr and True [False, True]
fiveC = foldr (&&) True [False, True]

-- d) foldr (||) True [False, True]
-- no

-- e) foldl ((++) . show) "" [1..5]
fiveE = foldl (flip((++) . show)) "" [1..5]

-- f) foldr const 'a' [1..5]
fiveF = foldr (flip const) 'a' [1..5]

-- g) foldr const 0 "tacos"
fiveG = foldr (flip const) 0 "tacos"

-- h) foldl (flip const) 0 "burritos"
fiveH = foldl const 0 "burritos"

-- i) foldl (flip const) 'z' [1..5]
fiveI = foldl const 'z' [1..5]


-- Exercises: Database Processing

data DatabaseItem
  = DbString String
  | DbNumber Integer
  | DbDate UTCTime
  deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase =
  [ DbDate (UTCTime (fromGregorian 1911 5 1) (secondsToDiffTime 34123))
  , DbNumber 9001
  , DbString "Hello, World!"
  , DbDate (UTCTime (fromGregorian 1921 5 1) (secondsToDiffTime 34123))
  , DbNumber 9002
  ]

-- 1
filterDbDate :: [DatabaseItem] -> [UTCTime]
filterDbDate = foldr f []
  where
    f (DbDate a) b = a : b
    f _ b = b

-- 2
filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber = foldr f []
  where
    f (DbNumber a) b = a : b
    f _ b = b

-- 3
mostRecent :: [DatabaseItem] -> UTCTime
mostRecent = f . filterDbDate
  where
    f (x:xs) = foldr (\a b -> if a > b then a else b) x xs

-- 4
sumDb :: [DatabaseItem] -> Integer
sumDb xs = foldr (+) 0 $ filterDbNumber xs

-- 5
avgDb :: [DatabaseItem] -> Double
avgDb xs = fromIntegral sumNumbers / fromIntegral countNumbers
  where
    sumNumbers = sumDb xs
    countNumbers = length $ filterDbNumber xs


-- 10.9 (Scans)

fibs = 1 : scanl (+) 1 fibs

fibsN x = fibs !! x


-- Scan Exercises
-- 1
-- fibs20 = take 20 $ fibs
fibs20 = take 20 (1 : scanl (+) 1 fibs20)

-- 2
fibs100Less = takeWhile (<100) (1 : scanl (+) 1 fibs100Less)

-- 3
-- factorial :: Integer -> Integer
factorial n = take n $ scanl (*) 1 [2..]
