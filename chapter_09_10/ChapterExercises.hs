module ChapterExercises where

import Data.Char
import Data.Bool
import Data.List


-- Chapter 9

-- 1
-- toUpper :: Char -> Char
-- isUpper :: Char -> Bool

-- 2
removeLower :: String -> String
removeLower = filter isUpper

-- 3
capitalize :: String -> String
capitalize [] = []
capitalize (x:xs) = toUpper x : xs

-- 4
capitalize' :: String -> String
capitalize' [] = []
capitalize' (x:xs) = toUpper x : capitalize' xs
-- or capitalize' xs = map toUpper xs

-- 5
firstCapital :: String -> Char
firstCapital [] = ' '
firstCapital (x:xs) = head $ toUpper x : xs

-- 6
firstCapitalCP :: String -> Char
firstCapitalCP xs
  | xs == "" = ' '
  | otherwise = (head . map toUpper) xs

-- Using 'head' will throw exception on empty string
firstCapitalPF :: String -> Char
firstCapitalPF = head . map toUpper

-- 'take' will return empty string on empty string
firstCapitalPF' :: String -> String
firstCapitalPF' = take 1 . map toUpper


-- Writing your own standard functions
-- Examples

-- direct recursion, not using (&&)
myAnd :: [Bool] -> Bool
myAnd [] = True
myAnd (x:xs) =
  if x == False
  then False
  else myAnd xs

-- direct recursion, using (&&)
myAnd' :: [Bool] -> Bool
myAnd' [] = True
myAnd' (x:xs) = x && myAnd xs

-- 1
myOr :: [Bool] -> Bool
myOr [] = False
myOr (x:xs) = x || myOr xs

-- 2
myAny :: (a -> Bool) -> [a] -> Bool
myAny _ [] = False
myAny f (x:xs) = f x || myAny f xs

-- 3
myElem :: Eq a => a -> [a] -> Bool
myElem _ [] = False
myElem y (x:xs) = y == x || myElem y xs

myElem' :: Eq a => a -> [a] -> Bool
myElem' y = any (y==)

-- 4
myReverse :: [a] -> [a]
myReverse xs = go xs []
  where
    go [] reversed = reversed
    go (x':xs') reversed = go xs' (x':reversed)

-- 5
squish :: [[a]] -> [a]
squish [] = []
squish (xs:xss) = xs ++ squish xss

-- 6
squishMap :: (a -> [b]) -> [a] -> [b]
squishMap _ [] = []
squishMap f (x:xs) = f x ++ squishMap f xs

-- 7
squishAgain :: [[a]] -> [a]
squishAgain = squishMap (\x->x)

-- 8
myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy f xs
  | length xs == 1 = heady
  | f heady (head taily) == GT = myMaximumBy f (heady : drop 1 taily)
  | otherwise = myMaximumBy f taily
  where
    heady = head xs
    taily = tail xs

-- 9
myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy f xs
  | length xs == 1 = heady
  | f heady (head taily) == LT = myMinimumBy f (heady : drop 1 taily)
  | otherwise = myMinimumBy f taily
  where
    heady = head xs
    taily = tail xs

-- 10
myMaximum :: (Ord a) => [a] -> a
myMaximum = myMaximumBy compare

myMinimum :: (Ord a) => [a] -> a
myMinimum = myMinimumBy compare



-- Chapter 10

-- 1
stops = "pbtdkg"
vowels = "aeiou"

-- a)
oneA :: [(Char, Char, Char)]
oneA = [(s,v,s') | s <- stops, v <- vowels, s' <- stops]

-- b)
oneB :: [(Char, Char, Char)]
oneB = [(s,v,s') | s <- stops, v <- vowels, s' <- stops, s == 'p']

-- c)
nouns = ["cat", "sock", "ship", "hero"]
verbs = ["be", "beat", "become", "begin"]

oneC :: [(String, String, String)]
oneC = [(n,v,n') | n <- nouns, v <- verbs, n' <- nouns]


-- 2
-- get a string, splits into words,
-- maps each word to it's length sum them up
-- and divide by the length of the splited words.
seekritFunc :: String -> Int
seekritFunc x =
  div (sum (map length (words x))) (length (words x))

-- 3
seekritFunc' :: String -> Double
seekritFunc' x =
  fromIntegral (sum (map length (words x))) / fromIntegral (length (words x))


-- Rewriting functions using folds

-- 1
myOr1 :: [Bool] -> Bool
myOr1 = foldr (||) False

myOr1' :: [Bool] -> Bool
myOr1' = foldr (\a b -> if a == True then True else b) False

-- 2
myAny2 :: (a -> Bool) -> [a] -> Bool
myAny2 f = foldr (\a b -> f a || b) False

myAny2' :: (a -> Bool) -> [a] -> Bool
myAny2' = \f -> foldr (\a b -> f a || b) False

-- 3
myElem3 :: Eq a => a -> [a] -> Bool
myElem3 = \x -> foldr (\a b -> a == x || b) False

myElem3' :: Eq a => a -> [a] -> Bool
myElem3' = \x -> myAny2' (x==)

-- 4
myReverse4 :: [a] -> [a]
myReverse4 = foldl (\b a -> a:b) []

-- 5
myMap5 :: (a -> b) -> [a] -> [b]
myMap5 = \f -> foldr (\a b -> f a:b) []

-- 6
myFilter6 :: (a -> Bool) -> [a] -> [a]
myFilter6 = \f -> foldr (\a b -> if f a then a:b else b) []

-- 7
squish7 :: [[a]] -> [a]
squish7 = foldr (++) []

-- 8
squishMap8 :: (a -> [b]) -> [a] -> [b]
squishMap8 = \f -> foldr (\a b -> f a ++ b) []

-- 9
squishAgain9 :: [[a]] -> [a]
squishAgain9 = squishMap8 (\x -> x)

-- 10
myMaximumBy10 :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy10 _ [] = error "empty list"
myMaximumBy10 f (x:xs) = foldr fn x xs
  where
    fn a b = if f a b == GT then a else b

-- 11
myMinimumBy10 :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy10 _ [] = error "empty list"
myMinimumBy10 f (x:xs) = foldr fn x xs
  where
    fn a b = if f a b == LT then a else b
