module ChapterExercises where

import Control.Monad
import System.Exit (exitSuccess)
import Data.Char


-- Modifying code

-- 1
-- TODO

-- 2
palindrome :: IO ()
palindrome = forever $ do
  line1 <- getLine
  case (line1 == reverse line1) of
    True -> putStrLn "It's a palindrome!"
    False -> do
      putStrLn "Nope! Bye!"
      exitSuccess

-- 3
palindrome2 :: IO ()
palindrome2 = forever $ do
  line1 <- getLine
  case (sanatizeText line1 == reverse (sanatizeText line1)) of
    True -> putStrLn "It's a palindrome!"
    False -> do
      putStrLn "Nope! Bye!"
      exitSuccess

sanatizeText :: String -> String
sanatizeText =
  map toLower .
  filter (\c -> isAlphaNum c && not (isSpace c))

-- 4
type Name = String
type Age = Integer

data Person = Person Name Age deriving Show

data PersonInvalid
  = NameEmpty
  | AgeEmpty
  | AgeTooLow
  | PersonInvalidUnknown String
  deriving (Eq, Show)

mkPerson :: Name -> Age -> Either PersonInvalid Person
mkPerson name age
  | name /= "" && age > 0 = Right $ Person name age
  | name == "" = Left NameEmpty
  | not (age > 0) = Left AgeTooLow
  | otherwise =
    Left $ PersonInvalidUnknown $
    "Name was: " ++ show name ++
    "Age was: " ++ show age

gimmePerson :: IO ()
gimmePerson = do
  putStr "Your name: "
  name <- getLine
  putStr "Your age: "
  age <- getLine
  case mkPerson name (read age) of
    Right person ->
      putStr "Yay! Successfully got a person: " >>= (\_ ->  print person)
    Left err ->
      putStr "Error: " >>= (\_ -> print err)
