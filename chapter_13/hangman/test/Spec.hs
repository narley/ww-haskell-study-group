{-# LANGUAGE FlexibleContexts #-}

import Test.QuickCheck
import Test.QuickCheck.Gen (oneof)
import Test.Hspec


-- data Puzzle = Puzzle WordToGuess CorrectLetters GuessedLetters

-- Can't make sense of what to test!!!

genAllowedChars :: Gen Char
genAllowedChars = elements ['a'..'z']

genWordToguess :: Gen [Char]
genWordToguess = listOf genAllowedChars

genCorrectLetters :: [char]
genCorrectLetters = undefined

genWordGuessed :: [Char] -> Gen [Char]
genWordGuessed word =
  listOf (genAllowedChars `suchThat` (`notElem` word))


main :: IO ()
main = do
  putStrLn " Test suite not implemented"
